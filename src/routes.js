import React from 'react';
import $ from 'jquery';

window.jQuery = $;
window.$ = $;
global.jQuery = $;

const DashboardDefault = React.lazy(() => import('./Demo/Dashboard/Default'));
const BusniessSetup = React.lazy(() => import('./views/Business/busniessSetup'));
const viewYourSite = React.lazy(() => import('./views/user-site/AppointmentSiteUi/test'));
const Calendar = React.lazy(() => import('./views/Calendar/Calendar'));
const Appointments = React.lazy(() => import('./views/user-site/AppointmentSiteUi/AppointmentSiteUi'));
const MonthAppointments = React.lazy(() => import('./views/Appointments/month'));
const TodayAppointments = React.lazy(() => import('./views/Appointments/today'));
const WeekAppointments = React.lazy(() => import('./views/Appointments/week'));
const CustomAppointments = React.lazy(() => import('./views/Appointments/customAppointments'));
const AddCustomer = React.lazy(() => import('./views/Customers/AddCustomer'));
const CustomerListing = React.lazy(() => import('./views/Customers/CustomerListing'));
const AddStaff = React.lazy(() => import('./views/Staff/AddStaff'));
const StaffListing = React.lazy(() => import('./views/Staff/StaffListing'));
const AddService = React.lazy(() => import('./views/Services/AddService'));
const ServiceListing = React.lazy(() => import('./views/Services/ServiceListing'));
const AddResource = React.lazy(() => import('./views/Resources/AddResource'));
const ResourceListing = React.lazy(() => import('./views/Resources/ResourceListing'));
const RolesPermissions = React.lazy(() => import('./views/Roles&Permissions/RolesPermissions'));
const NewTextMessage = React.lazy(() => import('./views/Messages/NewTextMessage'));
const TextMessageListing = React.lazy(() => import('./views/Messages/TextMessageListing'));
const AddNewLocation = React.lazy(() => import('./views/Locations/AddNewLocation'));
const LocationListing = React.lazy(() => import('./views/Locations/LocationListing'));



const routes = [
    { path: '/dashboard/default', exact: true, name: 'Default', component: DashboardDefault },
    { path: '/busniessSetup', exact: true, name: 'Busniess Setup', component: BusniessSetup },
    { path: '/viewyoursite', exact: true, name: 'View Your Site', component: viewYourSite },
    { path: '/Calendar', exact: true, name: 'Calendar', component: Calendar },
    { path: '/Appointments', exact: true, name: 'Appointments', component: Appointments },
    { path: '/month', exact: true, name: 'Month', component: MonthAppointments },
    { path: '/today', exact: true, name: 'Today', component: TodayAppointments },
    { path: '/week', exact: true, name: 'Week', component: WeekAppointments },
    { path: '/customDate', exact: true, name: 'custom Date', component: CustomAppointments },
    { path: '/AddCustomer', exact: true, name: 'Add Customer', component: AddCustomer },
    { path: '/CustomerListing', exact: true, name: 'Customer Listing', component: CustomerListing },
    { path: '/AddStaff', exact: true, name: 'Add Staff', component: AddStaff },
    { path: '/StaffListing', exact: true, name: 'Staff Listing', component: StaffListing },
    { path: '/AddService', exact: true, name: 'Add Service', component: AddService },
    { path: '/ServiceListing', exact: true, name: 'Service Listing', component: ServiceListing },
    { path: '/AddResource', exact: true, name: 'Add Resource', component: AddResource },
    { path: '/ResourceListing', exact: true, name: 'Resource Listing', component: ResourceListing },
    { path: '/RolesPermissions', exact: true, name: 'Roles and Permissions', component: RolesPermissions },
    { path: '/NewTextMessage', exact: true, name: 'New Text Message', component: NewTextMessage },
    { path: '/TextMessageListing', exact: true, name: 'Text Message Listing', component: TextMessageListing },
    { path: '/AddNewLocation', exact: true, name: 'Add New Location', component: AddNewLocation },
    { path: '/LocationListing', exact: true, name: 'Location Listing', component: LocationListing }
];

export default routes;