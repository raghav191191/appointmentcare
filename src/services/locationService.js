import axios from 'axios';
import {url} from '../config/url'
import decode from 'jwt-decode';
import AuthService from "./authService";

export default class LocationServices extends AuthService {
     constructor() {
          super();
      }
  

   addLocation(dataVo){
        return axios.post(`${url.ADD_LOCATION}`,dataVo,super.setTokenToRequest())
        .then(res => {
         return res.data;
        }).catch(err =>{
         console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
        })
    }

     getLocationData(ownerId){
          return axios.get(`${url.LOCATION_LIST + ownerId}`, super.setTokenToRequest())
          .then(res => {
           return res.data;
          }).catch(err =>{
           console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          })
      }

      delLocation(id) { 
          return axios.get(url.LOCATION_DEL + id, super.setTokenToRequest())
          .then(res => {
              return (res.data.data);
          }).catch(err => {
              console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          });
          
        }

        updateLocationInfoVo(dataVo) { 
            console.log('xxxx dataVo',dataVo);
            
          return axios.post(`${url.LOCATION_UPDATE+dataVo.id}`,dataVo, super.setTokenToRequest())
          .then(res => {
           return res.data;
          }).catch(err =>{
           console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          })
        
     }

}
