import React from 'react';
import {Row, Col, Card, Form, Button, Alert} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";
import ServicesServices from '../../services/serviceServices'
import ResourceServices from '../../services/resourceServices'

const Api = new ServicesServices()
const RApi = new ResourceServices()

class AddService extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
          errors: {},
          alertBox: false,
          actionMessage: '',
          actionStatus: '',
          serviceProviders:false,
          resourceList:[],
          moreData:[],
          resourceId: '',
          resourceQty: '',
        }
      }
    
    componentDidMount () {
      this.getResourcesList();
      this.getLoginUserInfo();
    }
    getLoginUserInfo=()=>{
      const ownerId = Api.getProfile().id;
      RApi.LoginUserInfo(ownerId)
          .then((result) => {
              this.setState({
                ownerFirstName:result.data[0].ownerFirstName,
                ownerLastName:result.data[0].ownerLastName
              })
             
              
          }).catch(err => {
              console.log('xxx', err);
          })

    }
    getResourcesList = () => {
      const ownerId = Api.getProfile().id;
      console.log('xxxxxxxxxxxxxxx id', ownerId);
      
      RApi.getResourcesData(ownerId)
          .then((result) => {
              console.log('xxxxxxxxx result', result.data);
              this.setState({
                resourceList:result.data
              })
             
              
          }).catch(err => {
              console.log('xxx', err);
          })
  }

  getResourceById = resourceId => {
  if (resourceId) {
    let resources = ''
    for (let i = 0; i < this.state.resourceList.length; i++) {
      if (resourceId == this.state.resourceList[i]._id) {
        resources =
          resources + ' ' + this.state.resourceList[i].resourceName 
      }
    }
    return resources
  }
}


    handleInputChange = event => {
      const target = event.target
      const value = target.value
      const name = target.name
      this.setState({
        [name]: value,
        isinvalid: ''
      })
    }
    handleCheckboxChange=()=>{
        console.log('xxxxxxxxx checko');
        
       this.setState({
        serviceProviders:!this.state.serviceProviders
       })
    }

    AddServices=()=>{
        const dataVo = {
            ownerId:Api.getProfile().id, //tokenId
            serviceName: this.state.serviceName,
            actualDuraction: this.state.actualDuraction,
            bufferDuraction: this.state.bufferDuraction,
            priceDoller: this.state.priceDoller,
            notes: this.state.notes,
            Description: this.state.Description,
            serviceProviders: this.state.serviceProviders,
            servicesResource:this.state.moreData
           
            
        }

        console.log('xxxx',dataVo);
     
            Api.addServices(dataVo)
        .then(result => {
            console.log('xxxx result', result);
            if (result.success) {
              console.log('xxxx result xxxx', result.data._id);
              this.addServiesResource(result.data._id);
                this.setState({
                  actionMessage: result.message,
                  actionStatus: 'success',
                  alertBox: true,
                })
                
                setTimeout(() => {
                  this.setState({
                    alertBox: false
                  })
                }, 2000)
             this.props.history.replace('/ServiceListing');
              } else {
                this.setState({
                  actionMessage: result.message,
                  actionStatus: 'warning',
                  alertBox: false,
                })
              }
            
           
        })
        .catch(err => {
          console.log('err', err)
        })
   
        
        

    }

    

    addMoreColumne = () => {
      let listVo = {
        'resourceId': this.state.resourceId,
        'resourceQty': this.state.resourceQty,
        'userID': Api.getProfile().id,
       
      }
      let moreData = this.state.moreData;
      moreData.push(listVo)
      this.setState({
        moreData: moreData,
      }, () => {
        console.log('xxxxxxxxxxxx moreData', this.state.moreData);
        
       // this.resetForm();
      })
    }
    resetForm = () => {
      this.setState({
        resourceId: '',
        resourceQty: '',
      })
    }

    removeRow = (index) => {
      let val = this.state.moreData;
      val.splice(index, 1);
      this.setState({moreData: val });
    }

    render() {

        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">Create a new Service</Card.Title>
                            </Card.Header>
                            <Card.Body>
                          
                            <Row>
                                    <Col md={6}>
                                        <Form>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Name</Form.Label>
                                                <Form.Control type="text" placeholder="Enter name" name="serviceName"  onChange={this.handleInputChange} />
                                               
                                            </Form.Group>

                                            <Form.Group controlId="formBasicName">
                                                <Form.Label>Actual Duration</Form.Label>
                                                <Form.Control type="number" placeholder="Actual Duration (mins)" name="actualDuraction"  onChange={this.handleInputChange} />
                                            </Form.Group>

                                            <Form.Group controlId="formBasicPhone">
                                                <Form.Label>Buffer</Form.Label>
                                                <Form.Control type="number" placeholder="Buffer (mins)" name="bufferDuraction"  onChange={this.handleInputChange} />
                                            </Form.Group>
                                            <Form.Group controlId="formBasicStreet1">
                                                <Form.Label>Price</Form.Label>
                                                <Form.Control type="number" placeholder="Price in Doller" name="priceDoller"  onChange={this.handleInputChange} />
                                            </Form.Group>
                                            <Form.Group controlId="formBasicStreet2">


                                            {this.state.moreData.map((array, index) =>
                                             <div>
                                            
                                             <Col md={6}>
                                             <Form.Group controlId="formBasicStreet2">
                                            <Form.Label>Resource</Form.Label>
                                            <Form.Control type="text" value={this.getResourceById(array.resourceId)} />
                                            </Form.Group>
                                            </Col>
                                            <Col md={6}>
                                            <Form.Group controlId="formBasicStreet2">
                                            <Form.Label>Qty</Form.Label>
                                            <Form.Control type="number" value={array.resourceQty} />
                                            </Form.Group>
                                            </Col>
                                            <h6 className="remove float-right" onClick={this.removeRow.bind(this, index)}>remove </h6>
                                             </div>
                                            )}
                                            <Form.Label>Add Resources</Form.Label>
                                                 <Form.Control as="select" name="resourceId" onChange={this.handleInputChange} value={this.state.resourceId}>
                                                 <option value="defult">Defult</option>
                                                 {this.state.resourceList.map((array, index) =>
                                                  <option value={array._id}>{array.resourceName}</option>
                                                  )}
                                               
                                                </Form.Control>
                                          
                                            <Form.Label>Qty</Form.Label>
                                            <Form.Control type="number" placeholder="Qty" name="resourceQty"  onChange={this.handleInputChange} value={this.state.resourceQty} />
                                         
                                            <Button   size='sm' onClick={this.addMoreColumne}>
                                            Add More
                                        </Button> 
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col md={6}>
                                    <Form.Group controlId="formBasicStreet2">
                                                <Form.Label>Notes</Form.Label>
                                                <Form.Control as="select" name="notes"  onChange={this.handleInputChange}>
                                                <option value="REGULAR">Regular</option>
                                                <option value="HOUSE_CALL">House call</option>
                                                <option value="PHONE_APPOINTMENT">Phone appointment</option>
                                                </Form.Control>
                                            </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                            <Form.Label>Description</Form.Label>
                                            <Form.Control as="textarea" rows="3" name="Description"  onChange={this.handleInputChange} />
                                        </Form.Group>

                                        <Col md={12}>
                                        <h6 className="mt-5">Service providers</h6>
                                        <hr/>
                                        <Form.Group>
                                            <Form.Check
                                                custom
                                                type="checkbox"
                                                id="ServiceProviders"
                                                label={this.state.ownerFirstName +' '+this.state.ownerLastName}
                                                name="serviceProviders"  onChange={this.handleCheckboxChange}
                                                checked={this.state.serviceProviders}
                                            />
                                        </Form.Group>
                                  
                                       
                                    </Col>
                                            <Form.Group controlId="formBasicFirstName">
                                           
                                            <Button variant="primary" onClick={this.AddServices}>
                                            Create Service
                                            </Button>
                                            <Button variant="danger">
                                            Cancle
                                            </Button>

                                            {this.state.alertBox ? (
                                                <Alert variant={this.state.actionStatus}>
                                                  {this.state.actionMessage}
                                                </Alert>
                                              ) : (
                                                ''
                                              )}
                                      
                                           
                                            </Form.Group>
                                      
                                      
                                    </Col>
                                </Row>
                              
                            </Card.Body>
                        </Card>
                       
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default AddService;
