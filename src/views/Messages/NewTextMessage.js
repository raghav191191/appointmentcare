import React from 'react';
import {Row, Col, Card, Form, Button, InputGroup, FormControl, DropdownButton, Dropdown} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";

class AddResource extends React.Component {

    render() {

        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">Send a Text Message</Card.Title>
                                <span>Send a text message to one or more emails</span>
                            </Card.Header>
                            <Card.Body>
                            <Row>
                                    <Col md={6}>
                                        <Form>
                                           

                                            <Form.Group controlId="exampleForm.ControlTextarea1">
                                            <Form.Label>To</Form.Label>
                                            <Form.Control as="textarea" rows="1" />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlTextarea1">
                                            <Form.Label>Message</Form.Label>
                                            <Form.Control as="textarea" rows="3" />
                                        </Form.Group>
                                           
                                        </Form>
                                    
                                            <Form.Group controlId="formBasicFirstName">
                                           
                                            <Button variant="primary">
                                            Send text message(s)
                                            </Button>
                                            <Button variant="danger">
                                            Cancle
                                            </Button>
                                           
                                            </Form.Group>
                                      
                                      
                                    </Col>
                                </Row>
                              
                            </Card.Body>
                        </Card>
                       
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default AddResource;
