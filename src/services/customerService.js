import axios from 'axios';
import {url} from '../config/url'
import decode from 'jwt-decode';
import AuthService from "./authService";

export default class CustomerService extends AuthService {
     constructor() {
          super();
      }
  

   addCustomer(customerVo){
        return axios.post(`${url.ADD_CUSTOMER}`,customerVo,super.setTokenToRequest())
        .then(res => {
         return res.data;
        }).catch(err =>{
         console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
        })
     }

     getCustomerData(ownerId){
          return axios.get(`${url.CUSTOMER_LIST + ownerId}`, super.setTokenToRequest())
          .then(res => {
           return res.data;
          }).catch(err =>{
           console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          })
     }

     delCustomerById(id) { 
          return axios.get(url.CUSTOMER_DEL + id, super.setTokenToRequest())
          .then(res => {
              return (res.data.data);
          }).catch(err => {
              console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          });
          
     }
     updateCustomerInfo(dataVo) { 
          return axios.post(`${url.CUSTOMER_UPDATE+dataVo.id}`,dataVo, super.setTokenToRequest())
          .then(res => {
           return res.data;
          }).catch(err =>{
           console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          })
        
     }
    

    





}
