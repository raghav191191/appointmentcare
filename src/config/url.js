const domain = 'http://localhost:3300';
//const domain = 'https://appointmentcare.herokuapp.com'

exports.url = {
  ADD_USERS: ` ${domain}/register/user`,
  USER_LOGIN: `${domain}/login/authenticate`,
  GET_USER_INFO: `${domain}/login/userInfo/`,
  UPDATE_BUSNIESS: `${domain}/business/setup-update`,
  GET_DETAILS_BUSNIESS: `${domain}/business/business-details/`,

  //customer
  ADD_CUSTOMER: ` ${domain}/customer/add`,
  CUSTOMER_LIST: ` ${domain}/customer/list/`,
  CUSTOMER_DEL: ` ${domain}/customer/del/`,
  CUSTOMER_UPDATE: `${domain}/customer/update/`,


  //Location
  ADD_LOCATION: ` ${domain}/location/add`,
  LOCATION_LIST: ` ${domain}/location/list/`,
  LOCATION_DEL: ` ${domain}/location/del/`,
  LOCATION_UPDATE: `${domain}/location/update/`,


  //Resource
  ADD_RESOURCE: ` ${domain}/resource/add`,
  RESOURCES_LIST: ` ${domain}/resource/list/`,
  RESOURCES_DEL: ` ${domain}/resource/del/`,
  RESOURCES_UPDATE: `${domain}/resource/update/`,
  //services
  ADD_SERVICES: ` ${domain}/services/add`,
  SERVICES_LIST: ` ${domain}/services/list/`,
  SERVICES_DEL: ` ${domain}/services/del/`,
  SERVICES_UPDATE: `${domain}/services/update/`,
  ADD_SERVICES_RESOURCE: `${domain}/services/addServiceResource`,

  //APPOINTMENT
  ADD_APPOINTMENT: ` ${domain}/appointment/add`,
  APPOINTMENT_LIST: ` ${domain}/appointment/list/`,
  APPOINTMENT_DEL: ` ${domain}/appointment/del/`,
  APPOINTMENT_UPDATE: `${domain}/appointment/update/`,
  ADD_BREAK: ` ${domain}/appointment/addBreak`,
  BREAK_LIST: ` ${domain}/appointment/listBreak/`,
  BREAK_UPDATE: ` ${domain}/appointment/updateBreak/`,
  BREAK_DEL: ` ${domain}/appointment/delBreak/`,
  APPOINTMENT_UPDATE_BY_ID: `${domain}/appointment/edit/`,

  //Staff
  ADD_STAFF: `${domain}/staff/add`,
  LIST_STAFF: `${domain}/staff/list/`,
  STAFF_UPDATE: `${domain}/staff/update/`,
  STAFF_DELETE: `${domain}/staff/delete/`,


  //Site
  DIRECTORYLIST: `${domain}/site/directoryList`,






  /* ********************************** Business Site User ********************************* */

  BUSINESS_SITE_REGISTER: `${domain}/business/register/user`,
  BUSINESS_SITE_LOGIN: `${domain}/business/login/authenticate`

}
