export default {
    items: [
        {
            id: 'navigation',
            title: 'Navigation',
            type: 'group',
            icon: 'icon-navigation',
            children: [
                {
                    id: 'dashboard',
                    title: 'Dashboard',
                    type: 'item',
                    url: '/dashboard/default',
                    icon: 'feather icon-home',
                },
                {
                    id: 'Calendar',
                    title: 'Calendar',
                    type: 'item',
                    url: '/Calendar',
                    icon: 'feather icon-calendar',
                },
                {
                    id: 'Appointments',
                    title: 'Appointments',
                    type: 'collapse',
                    icon: 'feather icon-clock',
                    children: [
                        {
                            id: 'today',
                            title: 'Today',
                            type: 'item',
                            url: '/today',
                            breadcrumbs: false
                        },
                        {
                            id: 'week',
                            title: 'This Week',
                            type: 'item',
                            url: '/week',
                            breadcrumbs: false
                        },
                       
                        {
                            id: 'month',
                            title: 'This Month',
                            type: 'item',
                            url: '/month',
                            breadcrumbs: false
                        },
                        {
                            id: 'custom',
                            title: 'Custom Dates',
                            type: 'item',
                            url: '/customDate',
                            breadcrumbs: false
                        },
                       
                    ]
                },
                {
                    id: 'Customer',
                    title: 'Customer',
                    type: 'collapse',
                    icon: 'feather icon-user-plus',
                    children: [
                        {
                            id: 'AddCustomer',
                            title: 'Add Customer',
                            type: 'item',
                            url: '/AddCustomer',
                            breadcrumbs: false
                        },
                        {
                            id: 'CustomerListing',
                            title: 'Customer Listing',
                            type: 'item',
                            url: '/CustomerListing',
                            breadcrumbs: false
                        },
                       
                    ]
                },
                {
                    id: 'Staff',
                    title: 'Staff',
                    type: 'collapse',
                    icon: 'feather icon-users',
                    children: [
                        {
                            id: 'AddStaff',
                            title: 'Add Staff',
                            type: 'item',
                            url: '/AddStaff',
                            breadcrumbs: false
                        },
                        {
                            id: 'StaffListing',
                            title: 'Staff Listing',
                            type: 'item',
                            url: '/StaffListing',
                            breadcrumbs: false
                        },
                       
                    ]
                },
                {
                    id: 'Service',
                    title: 'Service',
                    type: 'collapse',
                    icon: 'feather icon-globe',
                    children: [
                        {
                            id: 'AddService',
                            title: 'Add Service',
                            type: 'item',
                            url: '/AddService',
                            breadcrumbs: false
                        },
                        {
                            id: 'ServiceListing',
                            title: 'Service Listing',
                            type: 'item',
                            url: '/ServiceListing',
                            breadcrumbs: false
                        },
                       
                    ]
                },
                {
                    id: 'Resource',
                    title: 'Resource',
                    type: 'collapse',
                    icon: 'feather icon-codepen',
                    children: [
                        {
                            id: 'AddResource',
                            title: 'Add Resource',
                            type: 'item',
                            url: '/AddResource',
                            breadcrumbs: false
                        },
                        {
                            id: 'ResourceListing',
                            title: 'Resource Listing',
                            type: 'item',
                            url: '/ResourceListing',
                            breadcrumbs: false
                        },
                       
                    ]
                },
                {
                    id: 'TextMessage',
                    title: 'Text Message',
                    type: 'collapse',
                    icon: 'feather icon-message-square',
                    children: [
                        {
                            id: 'NewTextMessage',
                            title: 'New Text Message',
                            type: 'item',
                            url: '/NewTextMessage',
                            breadcrumbs: false
                        },
                        {
                            id: 'TextMessageListing',
                            title: 'Text Message Listing',
                            type: 'item',
                            url: '/TextMessageListing',
                            breadcrumbs: false
                        },
                       
                    ]
                },
                {
                    id: 'Locations',
                    title: 'Locations',
                    type: 'collapse',
                    icon: 'feather icon-map-pin',
                    children: [
                        {
                            id: 'AddNewLocation',
                            title: 'Add New Location',
                            type: 'item',
                            url: '/AddNewLocation',
                            breadcrumbs: false
                        },
                        {
                            id: 'LocationListing',
                            title: 'Location Listing',
                            type: 'item',
                            url: '/LocationListing',
                            breadcrumbs: false
                        },
                       
                    ]
                },
                {
                    id: 'RolesPermissions',
                    title: 'Roles & Permissions',
                    type: 'item',
                    url: '/RolesPermissions',
                    icon: 'feather icon-bookmark',
                },
                {
                    id: 'busniessSetup',
                    title: 'Busniess Setup',
                    type: 'item',
                    url: '/busniessSetup',
                    icon: 'feather icon-clipboard',
                },
                {
                    id: 'viewYourSite',
                    title: 'View Your Site',
                    type: 'item',
                    url: '/viewyoursite',
                    icon: 'feather icon-clipboard',
                }
            ]
        },
        {
            id: 'pages',
            title: 'Pages',
            type: 'group',
            icon: 'icon-pages',
            children: [
                {
                    id: 'auth',
                    title: 'Authentication',
                    type: 'collapse',
                    icon: 'feather icon-lock',
                   
                    children: [
                        {
                            id: 'business',
                            title: 'Register business',
                            type: 'item',
                            url: '/auth/business',
                            breadcrumbs: false
                        },
                        {
                            id: 'signin-1',
                            title: 'Sign in',
                            type: 'item',
                            url: '/auth/signin-1',
                            breadcrumbs: false
                        },
                       
                    ]
                }
            ]
        }
    ]
}