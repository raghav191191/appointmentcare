import React from 'react';
import {Link} from 'react-router-dom'
import {Row, Col, Card,Navbar,Nav,NavDropdown} from 'react-bootstrap';
import Aux from "../../../hoc/_Aux";
import BusinessInfoService from '../../../services/busniessSetupService';

import './AppointmentSiteUi.css'

const BusinessInfoServiceApi = new BusinessInfoService();

class AppointmentSiteUi extends React.Component {

    constructor(props){
        super(props)
        this.state={
            businessInfo:[],
            sideMenuStatus:false
        }
    }

    componentDidMount(){

        if(this.props.userId){
            this.getBusinessInfo(this.props.userId);
        }
       
    }

    getBusinessInfo=(userId)=>{
        BusinessInfoServiceApi.getBusniessDetails(userId)
        .then(result=>{

            console.log('resultresult',result)
            this.setState({
                businessInfo:result.body
            })
        })
    }

    menuButton=()=>{
        this.setState({
            sideMenuStatus:true
        })
    }

    render() {

        return (
            <Aux>
            {!(this.state.businessInfo.length==0) ?
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
  <Navbar.Brand href="#home">{this.state.businessInfo[0].businessName}</Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="mr-auto">
      
      
    </Nav>
    <Nav className="mainMenu">
    <NavDropdown title="Menu" id="collasible-nav-dropdown">
    <NavDropdown.Item><Link to={'/business/'+ this.state.businessInfo[0].directoryName +'/user/login'}>Login</Link></NavDropdown.Item>
    <NavDropdown.Item ><Link to={'/business/'+ this.state.businessInfo[0].directoryName +'/user/register'}>Register</Link></NavDropdown.Item>
  
  </NavDropdown>
    </Nav>
  </Navbar.Collapse>
</Navbar>
:''}

<h2 className="text-center">Welcome to Appointment site</h2>

             
            </Aux>
        );
    }
}

export default AppointmentSiteUi;
