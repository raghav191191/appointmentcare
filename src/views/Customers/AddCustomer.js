import React from 'react';
import { Row, Col, Card, Form, Button, InputGroup, Alert, FormControl, DropdownButton, Dropdown } from 'react-bootstrap';

import Aux from "../../hoc/_Aux";

import CustomerService from '../../services/customerService'

const Api = new CustomerService() 

class AddCustomer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            receiveSmsN: false,
            receiveEmailN: false
        };
    }


    getInputTextValue = event => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value,
            isinvalid: '',
        })
    }

    creatCustomer = () => {
        const customerVo = {
            ownerId: Api.getProfile().id, //tokenId
            customerEmail: this.state.customerEmail,
            customerFirstName: this.state.customerFirstName,
            custmerLastName: this.state.custmerLastName,
            homePhone: this.state.homePhone,
            mobileNumber: this.state.mobileNumber,
            notes: this.state.notes,
            receiveSmsN: this.state.receiveSmsN,
            receiveEmailN: this.state.receiveEmailN,
            password: this.state.password,
        }

        Api.addCustomer(customerVo)
            .then(result => {
                this.setState({
                    actionMessage: result.message,
                    actionStatus: 'success',
                    alertBox: true,
                })
                setTimeout(() => {
                    this.setState({
                        alertBox: false
                    })
                    this.props.history.push('/CustomerListing')
                }, 2000)

            })
            .catch(err => {
                console.log('err', err)
            })

    }

    checkBoxChange = event => {
        const target = event.target
        const name = target.name

        if (name == "receiveSmsN") {
            this.setState({
                receiveSmsN: !this.state.receiveSmsN,
            });
        } else {
            this.setState({
                receiveEmailN: !this.state.receiveEmailN,
            });
        }
    }

    render() {

        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">Add New Customer</Card.Title>
                            </Card.Header>
                            <Card.Body>

                                <Row>
                                    <Col md={6}>
                                        <Form>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Customer Email</Form.Label>
                                                <Form.Control type="email" placeholder="Enter email"
                                                    name='customerEmail'
                                                    onChange={this.getInputTextValue}
                                                />
                                                <Form.Text className="text-muted">
                                                    We'll never share your email with anyone else.
                                                </Form.Text>
                                            </Form.Group>
                                            <Form.Group>
                                            <Form.Label>Password</Form.Label>
                                            <Form.Control type="password" placeholder="password"
                                                name='password'
                                                onChange={this.getInputTextValue}
                                            />
                                        </Form.Group>

                                            <Form.Group controlId="formBasicName">
                                                <Form.Label>Customer Firstname</Form.Label>
                                                <Form.Control type="text" placeholder="Firstname"
                                                    name='customerFirstName'
                                                    onChange={this.getInputTextValue}
                                                />
                                            </Form.Group>

                                            <Form.Group controlId="formBasicPhone">
                                                <Form.Label>Customer Lastname</Form.Label>
                                                <Form.Control type="text" placeholder="Lastname"
                                                    name='custmerLastName'
                                                    onChange={this.getInputTextValue}
                                                />
                                            </Form.Group>
                                            <Form.Group controlId="formBasicStreet1">
                                                <Form.Label>Home Phone</Form.Label>
                                                <Form.Control type="text" placeholder="phone Number"
                                                    name='homePhone'
                                                    onChange={this.getInputTextValue}
                                                />
                                            </Form.Group>
                                            <Form.Group controlId="formBasicStreet2">
                                                <Form.Label>Mobile Number</Form.Label>
                                                <Form.Control type="text" placeholder="Mobile number"
                                                    name='mobileNumber'
                                                    onChange={this.getInputTextValue}
                                                />
                                            </Form.Group>

                                        </Form>
                                    </Col>
                                    <Col md={6}>
                                        <Form.Group controlId="exampleForm.ControlTextarea1">
                                            <Form.Label>Notes</Form.Label>
                                            <Form.Control as="textarea" rows="3"
                                                name='notes'
                                                onChange={this.getInputTextValue}
                                            />
                                        </Form.Group>

                                        <Col md={12}>
                                            <h6 className="mt-5">Notification settings</h6>
                                            <hr />
                                            <Form.Group>
                                                <Form.Check
                                                    custom
                                                    type="checkbox"
                                                    id="checkbox1"
                                                    name="receiveSmsN"
                                                    label="Receive SMS notifications on appointment changes "
                                                    onChange={this.checkBoxChange}
                                                />
                                            </Form.Group>


                                            <Form.Group>
                                                <Form.Check
                                                    custom
                                                    type="checkbox"
                                                    id="checkbox2"
                                                    name="receiveEmailN"
                                                    label="Receive email notifications on appointment changes"
                                                    onChange={this.checkBoxChange}
                                                />
                                            </Form.Group>
                                        </Col>
                                        <Form.Group controlId="formBasicFirstName">

                                            <Button variant="primary" onClick={this.creatCustomer}>
                                                Create Customer
                                            </Button>
                                            <Button variant="danger">
                                                Cancle
                                            </Button>

                                        </Form.Group>

                                        {this.state.alertBox ? (
                                            <Alert variant={this.state.actionStatus}>
                                                {this.state.actionMessage}
                                            </Alert>
                                        ) : (
                                                ''
                                            )}

                                    </Col>
                                </Row>

                            </Card.Body>
                        </Card>

                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default AddCustomer;
