import React from 'react';
import { Row, Col, Tabs, Tab, Nav, Alert, Card, Form, Button, Table } from 'react-bootstrap';

import Aux from "../../hoc/_Aux";

import StaffService from '../../services/staffService';

const StaffServiceApi = new StaffService();


class AddStaff extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isinvalid: '',
            errors: {},
            alertBox: false,
            serviceProvider: false,
            schedular: false,
            providedServicesDefault: false,
            receiveSmsN: false,
            receiveEmailN: false,
            locationDefault: false,
            defaultCalender: [],
            appointmentTimeSlot: 15
        };
    }

    componentDidMount() {
        this.defaultStaffScheduler()
    }

    defaultStaffScheduler = () => {

        let defaultCalender = [
            {
                id: 1,
                name: 'Monday',
                startTime: {
                    id: 11,
                    hour: '9',
                    minute: '00',
                    duration: 'AM'
                },
                endTime: {
                    id: 12,
                    hour: '9',
                    minute: '00',
                    duration: 'AM'
                },
                off: false

            },
            {
                id: 2,
                name: 'Tuesday',
                startTime: {
                    id: 13,
                    hour: '9',
                    minute: '00',
                    duration: 'AM'
                },
                endTime: {
                    id: 14,
                    hour: '9',
                    minute: '00',
                    duration: 'AM'
                },
                off: false
            },
            {
                id: 3,
                name: 'Wednesday',
                startTime: {
                    id: 15,
                    hour: '9',
                    minute: '00',
                    duration: 'AM'
                },
                endTime: {
                    id: 16,
                    hour: '9',
                    minute: '00',
                    duration: 'AM'
                },
                off: false
            },
            {
                id: 4,
                name: 'Thursday',
                startTime: {
                    id: 17,
                    hour: '9',
                    minute: '00',
                    duration: 'AM'
                },
                endTime: {
                    id: 18,
                    hour: '9',
                    minute: '00',
                    duration: 'AM'
                },
                off: false
            },
            {
                id: 5,
                name: 'Friday',
                startTime: {
                    id: 19,
                    hour: '9',
                    minute: '00',
                    duration: 'AM'
                },
                endTime: {
                    id: 20,
                    hour: '9',
                    minute: '00',
                    duration: 'AM'
                },
                off: false
            },
            {
                id: 6,
                name: 'Saturday',
                startTime: {
                    id: 21,
                    hour: '9',
                    minute: '00',
                    duration: 'AM'
                },
                endTime: {
                    id: 22,
                    hour: '9',
                    minute: '00',
                    duration: 'AM'
                },
                off: true
            },
            {
                id: 7,
                name: 'Sunday',
                startTime: {
                    id: 22,
                    hour: '9',
                    minute: '00',
                    duration: 'AM'
                },
                endTime: {
                    id: 23,
                    hour: '9',
                    minute: '00',
                    duration: 'AM'
                },
                off: true
            },
        ]

        this.setState({
            defaultCalender: defaultCalender
        })
    }

    getInputTextValue = event => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value,
            isinvalid: '',
        })
    }

    checkBoxChange = event => {
        const target = event.target
        const name = target.name

        switch (name) {
            case 'serviceProvider': this.setState({
                serviceProvider: !this.state.serviceProvider,
            });
                break;
            case 'schedular': this.setState({
                schedular: !this.state.schedular,
            });
                break;
            case 'providedServicesDefault': this.setState({
                providedServicesDefault: !this.state.providedServicesDefault,
            });
                break;
            case 'receiveSmsN': this.setState({
                receiveSmsN: !this.state.receiveSmsN,
            });
                break;
            case 'receiveEmailN': this.setState({
                receiveEmailN: !this.state.receiveEmailN,
            });
                break;
            case 'locationDefault': this.setState({
                locationDefault: !this.state.locationDefault,
            });
                break;
        }
    }

    handleValidation() {
        let errors = {}
        let formIsValid = true
        // Name

        // Email
        if (!this.state.email) {
            formIsValid = false
            errors['email'] = 'Cannot be empty'
        }
        if (typeof this.state.email !== 'undefined') {
            let lastAtPos = this.state.email.lastIndexOf('@')
            let lastDotPos = this.state.email.lastIndexOf('.')
            if (
                !(
                    lastAtPos < lastDotPos &&
                    lastAtPos > 0 &&
                    this.state.email.indexOf('@@') == -1 &&
                    lastDotPos > 2 &&
                    this.state.email.length - lastDotPos > 2
                )
            ) {
                formIsValid = false
                errors['email'] = 'Email is not valid'
            }
        }


        this.setState({ errors: errors })
        return formIsValid
    }
    addStaff = () => {

        if (this.handleValidation()) {
            const userId = StaffServiceApi.getProfile().id
            const staffVo = {
                ownerId: userId,
                email: this.state.email,
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                homePhone: this.state.homePhone,
                mobileNumber: this.state.mobileNumber,
                serviceProvider: this.state.serviceProvider,
                schedular: this.state.schedular,
                providedServicesDefault: this.state.providedServicesDefault,
                receiveSmsN: this.state.receiveSmsN,
                receiveEmailN: this.state.receiveEmailN,
                locationDefault: this.state.locationDefault,

                weeklySchedule: {
                    defaultCalender: this.state.defaultCalender,
                    duration: this.state.appointmentTimeSlot
                }

            }

            console.log('staffVo',staffVo)
            StaffServiceApi.addStaff(staffVo)
                .then(result => {
                    this.setState({
                        actionMessage: result.message,
                        actionStatus: 'success',
                        alertBox: true,
                    })
                    setTimeout(() => {
                        this.setState({
                            alertBox: false
                        })
                        this.props.history.push('/StaffListing')
                    }, 2000)
                })
                .catch(err => {

                    console.log('error', err)
                })

        }


    }

    getValueStartTime = (e, id, slot) => {
        let p1 = this.state.defaultCalender.map(item => {
            var temp = Object.assign({}, item);
            if (temp.id === id) {
                if (slot == "hour") {
                    temp.startTime.hour = e.target.value
                }
                if (slot == "minute") {
                    temp.startTime.minute = e.target.value
                }
                if (slot == "duration") {
                    temp.startTime.duration = e.target.value
                }

            }
            return temp;
        });
        this.setState({
            defaultCalender: p1
        })
    }

    getValueEndTime = (e, id, slot) => {
        let p1 = this.state.defaultCalender.map(item => {
            var temp = Object.assign({}, item);
            if (temp.id === id) {
                if (slot == "hour") {
                    temp.endTime.hour = e.target.value
                }
                if (slot == "minute") {
                    temp.endTime.minute = e.target.value
                }
                if (slot == "duration") {
                    temp.endTime.duration = e.target.value
                }

            }
            return temp;
        });


        this.setState({
            defaultCalender: p1
        })
    }
    offDayCheckBoxChange = (id) => {
        let p1 = this.state.defaultCalender.map(item => {
            var temp = Object.assign({}, item);
            if (temp.id === id) {
                temp.off = !temp.off
            }
            return temp;
        });
        this.setState({
            defaultCalender: p1
        })

    }

    addStaffSchedule = () => {
        console.log('appointmentTimeSlot', this.state.appointmentTimeSlot)
        console.log('defaultCalender', this.state.defaultCalender)
    }
    acceptAppointmentTime = (timeSlot) => {
        this.setState({
            appointmentTimeSlot: timeSlot
        })
    }
    render() {
        return (
            <Aux>
                <Row>
                    <Col>
                        <h5>Add Staff Member</h5>
                        <hr />
                        <Tabs defaultActiveKey="profile">

                            <Tab eventKey="profile" title="PROFILE">
                                <Row>
                                    <Col md={6}>
                                        <Form>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Email</Form.Label>
                                                <Form.Control type="email" placeholder="Enter email" name="email" onChange={this.getInputTextValue} />
                                                <Form.Text className="text-muted">
                                                    We'll never share your email with anyone else.
                                                </Form.Text>
                                            </Form.Group>

                                            <span className='error-msg'>
                                                {this.state.errors.email}
                                            </span>

                                            <Form.Group controlId="formBasicName">
                                                <Form.Label>Firstname</Form.Label>
                                                <Form.Control type="text" placeholder="Firstname" name="firstName" onChange={this.getInputTextValue} />
                                            </Form.Group>

                                            <Form.Group controlId="formBasicPhone">
                                                <Form.Label>Lastname</Form.Label>
                                                <Form.Control type="text" placeholder="Lastname" name="lastName" onChange={this.getInputTextValue} />
                                            </Form.Group>
                                            <Form.Group controlId="formBasicStreet1">
                                                <Form.Label>Home Phone</Form.Label>
                                                <Form.Control type="text" placeholder="phone Number" name="phoneNumber" onChange={this.getInputTextValue} />
                                            </Form.Group>
                                            <Form.Group controlId="formBasicStreet2">
                                                <Form.Label>Mobile Number</Form.Label>
                                                <Form.Control type="text" placeholder="Mobile number" name="mobileNumber" onChange={this.getInputTextValue} />
                                            </Form.Group>

                                        </Form>
                                    </Col>
                                    <Col md={6}>
                                        <Col md={12}>
                                            <h6 className="mt-5">Rules</h6>
                                            <hr />
                                            <Form.Group>
                                                <Form.Check
                                                    custom
                                                    type="checkbox"
                                                    id="Serviceprovider"
                                                    name="serviceProvider"
                                                    label="Service provider"
                                                    onChange={this.checkBoxChange}
                                                />
                                            </Form.Group>


                                            <Form.Group>
                                                <Form.Check
                                                    custom
                                                    type="checkbox"
                                                    id="Scheduler"
                                                    name="schedular"
                                                    label="Scheduler"
                                                    onChange={this.checkBoxChange}
                                                />
                                            </Form.Group>
                                        </Col>
                                        <Col md={12}>
                                            <h6 className="mt-5">Provided services</h6>
                                            <hr />
                                            <Form.Group>
                                                <Form.Check
                                                    custom
                                                    type="checkbox"
                                                    id="provided_service"
                                                    name="providedServicesDefault"
                                                    label="Default"
                                                    onChange={this.checkBoxChange}
                                                />
                                            </Form.Group>

                                        </Col>

                                        <Col md={12}>
                                            <h6 className="mt-5">Notification settings</h6>
                                            <hr />
                                            <Form.Group>
                                                <Form.Check
                                                    custom
                                                    type="checkbox"
                                                    id="receive_sms"
                                                    name="receiveSmsN"
                                                    label="Receive SMS notifications on appointment changes "
                                                    onChange={this.checkBoxChange}
                                                />
                                            </Form.Group>


                                            <Form.Group>
                                                <Form.Check
                                                    custom
                                                    type="checkbox"
                                                    id="receive_email"
                                                    name="receiveEmailN"
                                                    label="Receive email notifications on appointment changes"
                                                    onChange={this.checkBoxChange}
                                                />
                                            </Form.Group>
                                        </Col>
                                        <Col md={12}>
                                            <h6 className="mt-5">Locations for this employee</h6>
                                            <hr />
                                            <Form.Group>
                                                <Form.Check
                                                    custom
                                                    type="checkbox"
                                                    id="location_employee"
                                                    name="locationDefault"
                                                    label="Default"
                                                    onChange={this.checkBoxChange}
                                                />
                                            </Form.Group>

                                        </Col>
                                        {/* <Form.Group controlId="formBasicFirstName">

                                            <Button variant="primary" onClick={this.addStaff}>
                                                Create Staff
                                            </Button>
                                            <Button variant="danger">
                                                Cancle
                                            </Button>

                                        </Form.Group> */}


                                    </Col>
                                </Row>
                            </Tab>
                            <Tab eventKey="home" title="WEEKLY SCHEDULE">
                                <Col md={12}>
                                    <h6 className="mt-5">Schedule for: Default</h6>
                                    <hr />
                                    <Form.Group>
                                        <Table striped bordered hover size="sm">
                                            <tr>
                                                <th>Day</th>
                                                <th>Start</th>
                                                <th>End</th>
                                                <th>Off?</th>
                                            </tr>
                                            {this.state.defaultCalender.map((value, index) => (

                                                <tr>
                                                    <td>{value.name}</td>
                                                    <td>
                                                        <select onChange={(e) => this.getValueStartTime(e, value.id, 'hour')}>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>:
                                               <select onChange={(e) => this.getValueStartTime(e, value.id, 'minute')}>
                                                            <option value="00">00</option>
                                                            <option value="05">05</option>
                                                            <option value="10">10</option>
                                                            <option value="15">15</option>
                                                        </select>:
                                               <select onChange={(e) => this.getValueStartTime(e, value.id, 'duration')}>
                                                            <option value="AM">AM</option>
                                                            <option value="PM">PM</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select onChange={(e) => this.getValueEndTime(e, value.id, 'hour')}>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>:
                                               <select onChange={(e) => this.getValueEndTime(e, value.id, 'minute')}>
                                                            <option value="00">00</option>
                                                            <option value="05">05</option>
                                                            <option value="10">10</option>
                                                            <option value="15">15</option>
                                                        </select>:
                                               <select onChange={(e) => this.getValueEndTime(e, value.id, 'duration')}>
                                                            <option value="AM">AM</option>
                                                            <option value="PM">PM</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" checked={value.off} onChange={() => this.offDayCheckBoxChange(value.id)} />
                                                        {/* <Form.Check
                                                            custom
                                                            type="checkbox"
                                                            id={"k" + index}
                                                            name="off"
                                                            label=""
                                                            value='true'
                                                            onChange={() => this.offDayCheckBoxChange(value.id)}
                                                        /> */}
                                                    </td>
                                                </tr>

                                            ))}


                                        </Table>
                                    </Form.Group>

                                </Col>
                                <Col md={12}>
                                    <h6 className="mt-5">Accept appointment times in increments of:</h6>
                                    <hr />
                                    <Form.Group >
                                        <Form.Check
                                            custom
                                            type="radio"
                                            label="15 minutes (e.g., 09:00, 09:15, 09:30...)"
                                            name="supportedRadios"
                                            id="15minutes"
                                            onClick={() => this.acceptAppointmentTime(15)}
                                        />
                                        <Form.Check
                                            custom
                                            type="radio"
                                            label="30 minutes (e.g., 09:00, 09:30, 10:00...)"
                                            name="supportedRadios"
                                            id="30minutes"
                                            onClick={() => this.acceptAppointmentTime(30)}
                                        />
                                        <Form.Check
                                            custom
                                            type="radio"
                                            label="60 minutes (e.g., 09:00, 10:00, 11:00...)"
                                            name="supportedRadios"
                                            id="60minutes"
                                            onClick={() => this.acceptAppointmentTime(60)}
                                        />
                                    </Form.Group>
                                    {this.state.alertBox ? (
                                        <Alert variant={this.state.actionStatus}>
                                            {this.state.actionMessage}
                                        </Alert>
                                    ) : (
                                            ''
                                        )}


                                </Col>
                                {/* <Form.Group controlId="formBasicFirstName">

                                    <Button variant="primary" onClick={this.addStaffSchedule}>
                                        Create Staff Schedule
                                   </Button>
                                   
                                </Form.Group> */}
                            </Tab>

                        </Tabs>
                        <Form.Group controlId="formBasicFirstName">

                            <Button variant="primary" onClick={this.addStaff}>
                                Create Staff
                           </Button>
                            <Button variant="danger">
                                Cancle
                            </Button>

                        </Form.Group>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default AddStaff;
