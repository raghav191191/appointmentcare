import React from 'react';
import {Row, Col, Card, Form, Button,Alert, InputGroup, FormControl, DropdownButton, Dropdown} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";
import LocationService from '../../services/locationService'
import ResourceServices from '../../services/resourceServices'

const Api = new LocationService()
const RApi = new ResourceServices()


class AddLocation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            employeeLocation: false,
          
        };
    }

    componentDidMount () {
      
        this.getLoginUserInfo();
      }
      getLoginUserInfo=()=>{
        const ownerId = Api.getProfile().id;
        
        RApi.LoginUserInfo(ownerId)
            .then((result) => {
                this.setState({
                  ownerFirstName:result.data[0].ownerFirstName,
                  ownerLastName:result.data[0].ownerLastName
                })
            }).catch(err => {
                console.log('xxx', err);
            })
  
      }

    handleCheckboxChange=()=>{
      
        
       this.setState({
        employeeLocation:!this.state.employeeLocation
       })
    }
    getInputTextValue = event => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value,
            isinvalid: '',
        })
    }

    addLocationBtn = () => {
        const dataVo = {
            ownerId: Api.getProfile().id, //tokenId
            locationName: this.state.locationName,
            locationAddress1: this.state.locationAddress1,
            locationAddress2: this.state.locationAddress2,
            locationCity: this.state.locationCity,
            locationState: this.state.locationState,
            locationZipcode: this.state.locationZipcode,
            phoneNumber: this.state.phoneNumber,
            mobileNumber: this.state.mobileNumber,
            fax: this.state.fax,
            employeeLocation: this.state.employeeLocation
        }

        console.log('xxxxxx localtion',dataVo);
        

        Api.addLocation(dataVo)
            .then(result => {
                this.setState({
                    actionMessage: result.message,
                    actionStatus: 'success',
                    alertBox: true,
                })
                setTimeout(() => {
                    this.setState({
                        alertBox: false
                    })
                    this.props.history.push('/LocationListing')
                }, 2000)

            })
            .catch(err => {
                console.log('err', err)
            })

    }

    render() {

        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">Create a new Location</Card.Title>
                                <span>Add a new location offering for your business.</span>
                            </Card.Header>
                            <Card.Body>
                          
                            <Row>
                            <Col md={6}>
                                <Form>
                                    <Form.Group controlId="locationName">
                                        <Form.Label>Name</Form.Label>
                                        <Form.Control type="text" placeholder="Enter name"
                                            name='locationName'
                                            onChange={this.getInputTextValue}
                                        />
                                       
                                    </Form.Group>

                                    <Form.Group controlId="locationAddress1">
                                        <Form.Label>Address1</Form.Label>
                                        <Form.Control type="text" placeholder="Address1"
                                            name='locationAddress1'
                                            onChange={this.getInputTextValue}
                                        />
                                    </Form.Group>

                                    <Form.Group controlId="locationAddress2">
                                        <Form.Label>Address2</Form.Label>
                                        <Form.Control type="text" placeholder="Address2"
                                            name='locationAddress2'
                                            onChange={this.getInputTextValue}
                                        />
                                    </Form.Group>
                                    <Form.Group controlId="locationCity">
                                        <Form.Label>City</Form.Label>
                                        <Form.Control type="text" placeholder="City"
                                            name='locationCity'
                                            onChange={this.getInputTextValue}
                                        />
                                    </Form.Group>
                                    <Form.Group controlId="locationState">
                                        <Form.Label>State</Form.Label>
                                        <Form.Control type="text" placeholder="State"
                                            name='locationState'
                                            onChange={this.getInputTextValue}
                                        />
                                    </Form.Group>
                                    <Form.Group controlId="locationZipcode">
                                    <Form.Label>Zip code</Form.Label>
                                    <Form.Control type="text" placeholder="Zip Code"
                                        name='locationZipcode'
                                        onChange={this.getInputTextValue}
                                    />
                                </Form.Group>

                                </Form>
                            </Col>
                            <Col md={6}>
                            <Form.Group controlId="phoneNumber">
                            <Form.Label>Phone Number</Form.Label>
                            <Form.Control type="text" placeholder="Phone number"
                                name='phoneNumber'
                                onChange={this.getInputTextValue}
                            />
                        </Form.Group>
                        <Form.Group controlId="mobileNumber">
                            <Form.Label>Mobile Number</Form.Label>
                            <Form.Control type="text" placeholder="State"
                                name='mobileNumber'
                                onChange={this.getInputTextValue}
                            />
                        </Form.Group>
                        <Form.Group controlId="fax">
                        <Form.Label>Fax</Form.Label>
                        <Form.Control type="text" placeholder="Fax"
                            name='fax'
                            onChange={this.getInputTextValue}
                        />
                    </Form.Group>

                                <Col md={12}>
                                    <h6 className="mt-5">Employees in location</h6>
                                    <hr />
                                    <Form.Group>
                                        <Form.Check
                                            custom
                                            type="checkbox"
                                            id="checkbox1"
                                            name="employeeLocation"
                                            label={this.state.ownerFirstName +' '+this.state.ownerLastName}
                                            onChange={this.handleCheckboxChange}
                                            checked={this.state.employeeLocation}
                                        />
                                    </Form.Group>


                                   
                                </Col>
                                <Form.Group controlId="formBasicFirstName">

                                    <Button variant="primary" onClick={this.addLocationBtn}>
                                        Add Location
                                    </Button>
                                    <Button variant="danger">
                                        Cancle
                                    </Button>

                                </Form.Group>

                                {this.state.alertBox ? (
                                    <Alert variant={this.state.actionStatus}>
                                        {this.state.actionMessage}
                                    </Alert>
                                ) : (
                                        ''
                                    )}

                            </Col>
                        </Row>
                              
                            </Card.Body>

                        </Card>
                       
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default AddLocation;
