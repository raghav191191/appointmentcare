import axios from 'axios';
import {url} from '../config/url'
import decode from 'jwt-decode';
import AuthService from "./authService";

export default class ServicesServices extends AuthService {
     constructor() {
          super();
      }
  

   addServices(dataVo){
        return axios.post(`${url.ADD_SERVICES}`,dataVo,super.setTokenToRequest())
        .then(res => {
         return res.data;
        }).catch(err =>{
         console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
        })
     }

     getServicesData(ownerId){
          return axios.get(`${url.SERVICES_LIST + ownerId}`, super.setTokenToRequest())
          .then(res => {
           return res.data;
          }).catch(err =>{
           console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          })
      }

      delServices(id) { 
          return axios.get(url.SERVICES_DEL + id, super.setTokenToRequest())
          .then(res => {
              return (res.data.data);
          }).catch(err => {
              console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          });
          
        }

        updateServicesInfo(dataVo) { 
          return axios.post(`${url.SERVICES_UPDATE+dataVo.id}`,dataVo, super.setTokenToRequest())
          .then(res => {
           return res.data;
          }).catch(err =>{
           console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          })
        
     }

    //  addServiceresource(dataVo){
    //     return axios.post(`${url.ADD_SERVICES_RESOURCE}`,dataVo,super.setTokenToRequest())
    //     .then(res => {
    //      return res.data;
    //     }).catch(err =>{
    //      console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
    //     })
    //  }

    

    





}
