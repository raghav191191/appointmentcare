import React from 'react';
import { Row, Col, Card, Form, Button, Modal, Alert,Table } from 'react-bootstrap';

import Aux from "../../hoc/_Aux";
import { MDBDataTable } from 'mdbreact';
import 'mdbreact/dist/css/mdb.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import ServicesServices from '../../services/serviceServices'
import ResourceServices from '../../services/resourceServices'
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
const Api = new ServicesServices()
const RApi = new ResourceServices()
let RowArray = [];
class ServiceListing extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      lists: [],
      editModel: false,
      viewModel: false,
      viewServicesResource:[],
      resourceList:[],
       moreData:[],
          resourceId: '',
          resourceQty: '',
    }

    this.handleClose = this.handleClose.bind(this);
    this.handleCloseView = this.handleCloseView.bind(this);
  }
  componentDidMount() {

    this.getServicesList();
    this.getResourcesList();
    this.getLoginUserInfo();

  }
  getLoginUserInfo=()=>{
    const ownerId = Api.getProfile().id;
    console.log('xxxxxxxxxxxxxxx id', ownerId);
    
    RApi.LoginUserInfo(ownerId)
        .then((result) => {
            console.log('xxxxxxxxx result xxx', result.data[0]);
            this.setState({
              ownerFirstName:result.data[0].ownerFirstName,
              ownerLastName:result.data[0].ownerLastName
            })
           
            
        }).catch(err => {
            console.log('xxx', err);
        })

  }

  getResourcesList = () => {
    const ownerId = Api.getProfile().id;
    
    RApi.getResourcesData(ownerId)
        .then((result) => {
            this.setState({
              resourceList:result.data
            })
           
            
        }).catch(err => {
            console.log('xxx', err);
        })
}
getResourceById = resourceId => {
  if (resourceId) {
    let resources = ''
    for (let i = 0; i < this.state.resourceList.length; i++) {
      if (resourceId == this.state.resourceList[i]._id) {
        resources =
          resources + ' ' + this.state.resourceList[i].resourceName
      }
    }
    return resources
  }
}
  getServicesList = () => {
    const ownerId = Api.getProfile().id;
    console.log('xxxxxxxxxxxxxxx id', ownerId);

    Api.getServicesData(ownerId)
      .then((result) => {
        console.log('xxxxxxxxx result', result.data);
        RowArray = [];
        for (let i = 0; i < result.data.length; i++) {
          RowArray.push({
            serviceName: result.data[i].serviceName,
            actualDuraction: result.data[i].actualDuraction,
            bufferDuraction: result.data[i].bufferDuraction,
            priceDoller: result.data[i].priceDoller,
            notes: result.data[i].notes,

            action: [<span
              className='iconSet' onClick={() => this.viewServices(result.data[i])}

            >
              <i className="feather icon-info"></i>

            </span>,
            <span
              className='iconSet' id="editResource" onClick={() => this.editResource(result.data[i])}

            >
              <i className="feather icon-edit"></i>

            </span>, <span className='iconSet' id="delResource" onClick={() => this.submitCorfirm(result.data[i]._id)} >
              <i className="feather icon-trash-2"></i>

            </span>]
          });
        }
        RowArray = RowArray.reverse()
        this.setState({ RowArray: RowArray });

      }).catch(err => {
        console.log('xxx', err);
      })
  }
  submitCorfirm = (id) => {
    confirmAlert({
      title: 'Confirm to Delete Service',
      message: 'Are you sure to do this.',
      buttons: [
        {
          label: 'Yes',
          onClick: () => this.delService(id)
          // onClick: () => alert('Click No')
        },
        {
          label: 'No',
          // onClick: () => alert('Click No')
        }
      ]
    });
  };

  delService = (id) => {
    Api.delServices(id)
      .then((result) => {
        this.getServicesList();
      }).catch(err => {
        console.log('xxx', err);
      })
  }

  editResource = (dataVo) => {
    console.log('xxxx--', dataVo);

    this.setState({
      editModel: !this.state.editModel,
      editId: dataVo._id,
      editServiceName: dataVo.serviceName,
      editActualDuraction: dataVo.actualDuraction,
      editBufferDuraction: dataVo.bufferDuraction,
      editPriceDoller: dataVo.priceDoller,
      editNotes: dataVo.notes,
      editServiceProviders: dataVo.serviceProviders,
      editDescription: dataVo.Description,
      moreData: dataVo.servicesResource,

    })
  }

  viewServices = (dataVo) => {
    console.log('xxxx--', dataVo);

    this.setState({
      viewModel: !this.state.viewModel,
      viewServiceName: dataVo.serviceName,
      viewActualDuraction: dataVo.actualDuraction,
      viewBufferDuraction: dataVo.bufferDuraction,
      viewPriceDoller: dataVo.priceDoller,
      viewNotes: dataVo.notes,
      viewServiceProviders: dataVo.serviceProviders,
      viewDescription: dataVo.Description,
      viewServicesResource: dataVo.servicesResource,

    })
  }

  handleClose = () => {
    this.setState({
      editModel: !this.state.editModel,

    })
  }
  handleCloseView = () => {
    this.setState({
      viewModel: !this.state.viewModel,

    })
  }
  handleCheckboxChange = () => {

    this.setState({
      editServiceProviders: !this.state.editServiceProviders
    })
  }
  handleInputChange = event => {
    const target = event.target
    const value = target.value
    const name = target.name
    this.setState({
      [name]: value,
      isinvalid: ''
    })
  }

  editServicesSubmit = () => {
    const resourceVo = {
      id: this.state.editId,
      ownerId: Api.getProfile().id,
      serviceName: this.state.editServiceName,
      actualDuraction: this.state.editActualDuraction,
      bufferDuraction: this.state.editBufferDuraction,
      priceDoller: this.state.editPriceDoller,
      notes: this.state.editNotes,
      serviceProviders: this.state.editServiceProviders,
      Description: this.state.editDescription,
      servicesResource:this.state.moreData




    }
    console.log('xxx--', resourceVo);

    Api.updateServicesInfo(resourceVo)
      .then(result => {
        if (result.success) {
          this.getServicesList();
          this.setState({
            editModel: !this.state.editModel,
          })
        }
      }).catch(err => {
        console.log('err', err)
      })
  }

  addMoreColumne = () => {
    let listVo = {
      'resourceId': this.state.resourceId,
      'resourceQty': this.state.resourceQty,
      'userID': Api.getProfile().id,
     
    }
    let moreData = this.state.moreData;
    moreData.push(listVo)
    this.setState({
      moreData: moreData,
    }, () => {
      console.log('xxxxxxxxxxxx moreData', this.state.moreData);
      
     // this.resetForm();
    })
  }
  resetForm = () => {
    this.setState({
      resourceId: '',
      resourceQty: '',
    })
  }

  removeRow = (index) => {
    let val = this.state.moreData;
    val.splice(index, 1);
    this.setState({moreData: val });
  }

  render() {
    console.log('xxxxxxxxxx --', this.state.viewServicesResource);
    
    const data = {
      columns: [
        {
          label: 'Service Name',
          field: 'serviceName',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Actual Duraction(min)',
          field: 'actualDuraction',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Buffer Duraction(min)',
          field: 'bufferDuraction',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Price($)',
          field: 'priceDoller',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Notes',
          field: 'notes',
          sort: 'asc',
          width: 150
        },

        {
          label: 'Action',
          field: 'action',
          sort: 'asc',
          width: 150
        },
      ],
      rows: RowArray
    }

    return (
      <Aux>
        <Row>
          <Col>
            <Card>
              <Card.Header>
                <Card.Title as="h5">Service Listing</Card.Title>
              </Card.Header>
              <Card.Body>

                <div>
                  <MDBDataTable
                    striped
                    data={data}
                    hover
                  />
                  <Modal show={this.state.editModel}>
                    <Modal.Header >
                      <Modal.Title>Resource update</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Form>
                        <Form.Group controlId="formBasicEmail">
                          <Form.Label>Name</Form.Label>
                          <Form.Control type="text" placeholder="Enter name" name="editServiceName" value={this.state.editServiceName} onChange={this.handleInputChange} />

                        </Form.Group>

                        <Form.Group controlId="formBasicName">
                          <Form.Label>Actual Duration</Form.Label>
                          <Form.Control type="number" placeholder="Actual Duration (mins)" name="editActualDuraction" value={this.state.editActualDuraction} onChange={this.handleInputChange} />
                        </Form.Group>

                        <Form.Group controlId="formBasicPhone">
                          <Form.Label>Buffer</Form.Label>
                          <Form.Control type="number" placeholder="Buffer (mins)" name="editBufferDuraction" onChange={this.handleInputChange} value={this.state.editBufferDuraction} />
                        </Form.Group>
                        <Form.Group controlId="formBasicStreet1">
                          <Form.Label>Price</Form.Label>
                          <Form.Control type="number" placeholder="Price in Doller" name="editPriceDoller" onChange={this.handleInputChange} value={this.state.editPriceDoller} />
                        </Form.Group>



                        <Form.Group controlId="formBasicStreet2">


                                            {this.state.moreData.map((array, index) =>
                                             <div>
                                            
                                             <Col md={6}>
                                             <Form.Group controlId="formBasicStreet2">
                                            <Form.Label>Resource</Form.Label>
                                            <Form.Control type="text" value={this.getResourceById(array.resourceId)} />
                                            </Form.Group>
                                            </Col>
                                            <Col md={6}>
                                            <Form.Group controlId="formBasicStreet2">
                                            <Form.Label>Qty</Form.Label>
                                            <Form.Control type="number" value={array.resourceQty} />
                                            </Form.Group>
                                            </Col>
                                            <h6 className="remove float-right" onClick={this.removeRow.bind(this, index)}>remove </h6>
                                             </div>
                                            )}
                                            <Form.Label>Add Resources</Form.Label>
                                                 <Form.Control as="select" name="resourceId" onChange={this.handleInputChange} value={this.state.resourceId}>
                                                 <option value="defult">Defult</option>
                                                 {this.state.resourceList.map((array, index) =>
                                                  <option value={array._id}>{array.resourceName}</option>
                                                  )}
                                               
                                                </Form.Control>
                                          
                                            <Form.Label>Qty</Form.Label>
                                            <Form.Control type="number" placeholder="Qty" name="resourceQty"  onChange={this.handleInputChange} value={this.state.resourceQty} />
                                         
                                            <Button   size='sm' onClick={this.addMoreColumne}>
                                            Add More
                                        </Button> 
                                            </Form.Group>




                       
                        <Form.Group controlId="formBasicStreet2">
                          <Form.Label>Notes</Form.Label>
                          <Form.Control as="select" name="editNotes" value={this.state.editNotes} onChange={this.handleInputChange}>
                            <option value="REGULAR">Regular</option>
                            <option value="HOUSE_CALL">House call</option>
                            <option value="PHONE_APPOINTMENT">Phone appointment</option>
                          </Form.Control>
                        </Form.Group>
                        <Form.Group controlId="exampleForm.ControlTextarea1">
                          <Form.Label>Description</Form.Label>
                          <Form.Control as="textarea" rows="3" name="editDescription" value={this.state.editDescription} onChange={this.handleInputChange} />
                        </Form.Group>


                        <h6 className="mt-5">Service providers</h6>
                        <hr />
                        <Form.Group>
                          <Form.Check
                            custom
                            type="checkbox"
                            id="ServiceProviders"
                            label={this.state.ownerFirstName +' '+this.state.ownerLastName}
                            name="editServiceProviders" onChange={this.handleCheckboxChange}
                            checked={this.state.editServiceProviders}
                          />
                        </Form.Group>

                      </Form>

                      <Form.Group controlId="formBasicFirstName">



                        {this.state.alertBox ? (
                          <Alert variant={this.state.actionStatus}>
                            {this.state.actionMessage}
                          </Alert>
                        ) : (
                            ''
                          )}


                      </Form.Group>


                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="secondary" onClick={this.handleClose}>
                        Close
          </Button>
                      <Button variant="primary" onClick={this.editServicesSubmit}>
                        Update Changes
          </Button>
                    </Modal.Footer>
                  </Modal>



                  <Modal show={this.state.viewModel}>
                    <Modal.Header >
                      <Modal.Title>Services Info</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Form>
                      <Table striped bordered hover size="sm">
                      
                      <tr>
                      <th>Services Name</th>
                      <td>{this.state.viewServiceName}</td>
                      </tr>
                      <tr>
                      <th>Actual Duraction</th>
                      <td>{this.state.viewActualDuraction}(min)</td>
                      </tr>
                      <tr>
                      <th>Buffer Duraction</th>
                      <td>{this.state.viewBufferDuraction}(min)</td>
                      </tr>
                      <tr>
                      <th>Doller</th>
                      <td>${this.state.viewPriceDoller}</td>
                      </tr>
                      <tr>
                      <th>Notes</th>
                      <td>{this.state.viewNotes}</td>
                      </tr>
                      <tr>
                      <th>Description</th>
                      <td>{this.state.viewDescription}</td>
                      </tr>
                      <tr>
                      <th>Resource & Qty</th>
                      <td>  {this.state.viewServicesResource.map((array, index) =>
                        <p>{this.getResourceById(array.resourceId)}({array.resourceQty})<br></br></p>
                        )}</td>
                      </tr>
                      </Table>
                      </Form>


                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="secondary" onClick={this.handleCloseView}>
                        Close
          </Button>
                     
                    </Modal.Footer>
                  </Modal>

                </div>

              </Card.Body>
            </Card>

          </Col>
        </Row>
      </Aux>
    );
  }
}

export default ServiceListing;
