import React from 'react';
import { Row, Col, Card, Form, Button, InputGroup, FormControl, DropdownButton, Dropdown, Modal, Table } from 'react-bootstrap';
import { MDBDataTable } from 'mdbreact';
import 'mdbreact/dist/css/mdb.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import Aux from "../../hoc/_Aux";
import StaffService from '../../services/staffService'
const StaffServiceApi = new StaffService()
let RowArray = [];

class StaffListing extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            editStaffModel: false,
            editStaffEmail: '',
            editStaffFirstName: '',
            editStaffLastName: '',
            editStaffHomePhone: '',
            editMobileNumber: '',
            editServiceProvider: '',
            editScheduler: '',
            editProvidedServicesDefault: '',
            editReceiveSmsN: '',
            editReceiveEmailN: '',
            editLocationDefault: '',

            viewStaffModel: false
        }
    }

    componentDidMount() {
        this.getStaffList()
    }


    getInputTextValue = event => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value,
            isinvalid: '',
        })
    }

    viewStaff = (staffDataVo) => {
        this.setState({
            viewStaffModel: !this.state.viewStaffModel,
            viewStaffId: staffDataVo._id,
            viewStaffEmail: staffDataVo.email,
            viewStaffFirstName: staffDataVo.firstName,
            viewStaffLastName: staffDataVo.lastName,
            viewStaffHomePhone: staffDataVo.homePhone,
            viewMobileNumber: staffDataVo.mobileNumber,
            viewServiceProvider: staffDataVo.serviceProvider,
            viewScheduler: staffDataVo.schedular,
            viewProvidedServicesDefault: staffDataVo.providedServicesDefault,
            viewReceiveSmsN: staffDataVo.receiveSmsN,
            viewReceiveEmailN: staffDataVo.receiveEmailN,
            viewLocationDefault: staffDataVo.locationDefault,


        })
    }
    editStaff = (staffDataVo) => {

        this.setState({
            editStaffModel: !this.state.editStaffModel,
            editStaffId: staffDataVo._id,
            editStaffEmail: staffDataVo.email,
            editStaffFirstName: staffDataVo.firstName,
            editStaffLastName: staffDataVo.lastName,
            editStaffHomePhone: staffDataVo.homePhone,
            editMobileNumber: staffDataVo.mobileNumber,
            editServiceProvider: staffDataVo.serviceProvider,
            editScheduler: staffDataVo.schedular,
            editProvidedServicesDefault: staffDataVo.providedServicesDefault,
            editReceiveSmsN: staffDataVo.receiveSmsN,
            editReceiveEmailN: staffDataVo.receiveEmailN,
            editLocationDefault: staffDataVo.locationDefault,


        })
        console.log('staffData', staffDataVo)
    }

    deleteStaffModelOpen = (staffInfo) => {

        console.log('staffInfostaffInfo', staffInfo)
        confirmAlert({
            title: 'Confirm to Delete Staff',
            message: 'Are you sure to do this.',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.deletStaff(staffInfo)
                },
                {
                    label: 'No',
                }
            ]
        });


    }

    deletStaff = (staffInfo) => {

        const staffVo = {
            id: staffInfo._id,
            ownerId: StaffServiceApi.getProfile().id,

        }
        StaffServiceApi.deleteStaff(staffVo)
            .then(result => {
                if (result.success) {
                    this.getStaffList()
                }
            })
            .catch(err => {
                console.log('err', err)
            })
    }
    getStaffList = () => {
        const ownerId = StaffServiceApi.getProfile().id
        StaffServiceApi.getStaffList(ownerId)
            .then(result => {
                RowArray = [];
                for (let i = 0; i < result.data.length; i++) {
                    RowArray.push({
                        firstName: result.data[i].firstName,
                        email: result.data[i].email,
                        mobileNumber: result.data[i].mobileNumber,


                        action: [
                            <span
                                className='iconSet' onClick={() => this.viewStaff(result.data[i])}
                            >
                                <i className="feather icon-info"></i>
                            </span>,
                            <span
                                className='iconSet' onClick={() => this.editStaff(result.data[i])} >
                                <i className="feather icon-edit"></i>
                            </span>, <span className='iconSet' onClick={() => this.deleteStaffModelOpen(result.data[i])} >
                                <i className="feather icon-trash-2"></i>

                            </span>]
                    });
                }
                //   RowArray = RowArray.reverse()
                console.log('RowArrayRowArray', RowArray)
                this.setState({ RowArray: RowArray });
            })
            .catch(err => {
                console.log('err', err)
            })

    }

    checkBoxChange = event => {
        const target = event.target
        const name = target.name

        switch (name) {
            case 'editServiceProvider': this.setState({
                editServiceProvider: !this.state.editServiceProvider,
            });
                break;
            case 'editScheduler': this.setState({
                editScheduler: !this.state.editScheduler,
            });
                break;
            case 'editProvidedServicesDefault': this.setState({
                editProvidedServicesDefault: !this.state.editProvidedServicesDefault,
            });
                break;
            case 'editReceiveSmsN': this.setState({
                editReceiveSmsN: !this.state.editReceiveSmsN,
            });
                break;
            case 'editReceiveEmailN': this.setState({
                editReceiveEmailN: !this.state.editReceiveEmailN,
            });
                break;
            case 'editLocationDefault': this.setState({
                editLocationDefault: !this.state.editLocationDefault,
            });
                break;
        }
    }

    editStaffSubmit = () => {

        const staffVo = {
            id: this.state.editStaffId,
            ownerId: StaffServiceApi.getProfile().id,
            email: this.state.editStaffEmail,
            firstName: this.state.editStaffFirstName,
            lastName: this.state.editStaffLastName,
            homePhone: this.state.editStaffHomePhone,
            mobileNumber: this.state.editMobileNumber,
            serviceProvider: this.state.editServiceProvider,
            schedular: this.state.editScheduler,
            providedServicesDefault: this.state.editProvidedServicesDefault,
            receiveSmsN: this.state.editReceiveSmsN,
            receiveEmailN: this.state.editReceiveEmailN,
            locationDefault: this.state.editLocationDefault,
        }

        StaffServiceApi.updateStaffInfo(staffVo)
            .then(result => {
                if (result.success) {
                    this.getStaffList()
                    this.editStaffModelClose();
                }
            })
            .catch(err => {
                console.log('err', err)
            })
    }

    editStaffModelClose = () => {
        this.setState({
            editStaffModel: !this.state.editStaffModel,
        })
    }
    
    viewStaffModelClose=()=>{

        this.setState({
            viewStaffModel:!this.state.viewStaffModel
        })
    }
    render() {
        const data = {
            columns: [
                {
                    label: 'FirstName',
                    field: 'firstName',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Email',
                    field: 'email',
                    sort: 'asc',
                    width: 150
                },
                ,
                {
                    label: 'MobileNumber',
                    field: 'mobileNumber',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 150
                },
            ],
            rows: RowArray
        }

        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">Current Staff</Card.Title>
                            </Card.Header>
                            <Card.Body>

                                <div>
                                    <MDBDataTable
                                        striped
                                        data={data}
                                        hover
                                    />
                                    <Modal show={this.state.editStaffModel}>
                                        <Modal.Header >
                                            <Modal.Title>Staff update</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                            <Form>
                                                <Form.Group controlId="formBasicEmail">
                                                    <Form.Label>Email</Form.Label>
                                                    <Form.Control type="email" placeholder="Enter email"
                                                        name='editStaffEmail' value={this.state.editStaffEmail}
                                                        onChange={this.getInputTextValue} readOnly
                                                    />
                                                    <Form.Text className="text-muted">
                                </Form.Text>
                                                </Form.Group>

                                                <Form.Group controlId="formBasicName">
                                                    <Form.Label>First Name</Form.Label>
                                                    <Form.Control type="text" placeholder="First Name" value={this.state.editStaffFirstName}
                                                        name='editStaffFirstName'
                                                        onChange={this.getInputTextValue}
                                                    />
                                                </Form.Group>

                                                <Form.Group controlId="formBasicName">
                                                    <Form.Label>Last Name</Form.Label>
                                                    <Form.Control type="text" placeholder="Last Name"
                                                        name='editStaffLastName'
                                                        value={this.state.editStaffLastName}
                                                        onChange={this.getInputTextValue}
                                                    />
                                                </Form.Group>

                                                <Form.Group controlId="formBasicName">
                                                    <Form.Label>Home Phone</Form.Label>
                                                    <Form.Control type="text" placeholder="Home Phone" value={this.state.editStaffLastName}
                                                        name='editStaffHomePhone'
                                                        onChange={this.getInputTextValue}
                                                    />
                                                </Form.Group>

                                                <Form.Group controlId="formBasicName">
                                                    <Form.Label>Mobile Number</Form.Label>
                                                    <Form.Control type="text" placeholder="Mobile Number" value={this.state.editStaffHomePhone}
                                                        name='editMobileNumber'
                                                        onChange={this.getInputTextValue}
                                                    />
                                                </Form.Group>

                                                <h6 className="mt-5">Rules</h6>
                                                <hr />
                                                <Form.Group>
                                                    <Form.Check
                                                        custom
                                                        type="checkbox"
                                                        id="checkbox1"
                                                        name="editServiceProvider"
                                                        label="Service provider"
                                                        onChange={this.checkBoxChange}
                                                        checked={this.state.editServiceProvider}
                                                    />
                                                    <Form.Check
                                                        custom
                                                        type="checkbox"
                                                        id="checkbox2"
                                                        name="editScheduler"
                                                        label="Scheduler"
                                                        onChange={this.checkBoxChange}
                                                        checked={this.state.editScheduler}
                                                    />
                                                </Form.Group>


                                                <h6 className="mt-5">Provided services</h6>
                                                <hr />
                                                <Form.Group>
                                                    <Form.Check
                                                        custom
                                                        type="checkbox"
                                                        id="checkbox3"
                                                        name="editProvidedServicesDefault"
                                                        label="Default"
                                                        onChange={this.checkBoxChange}
                                                        checked={this.state.editProvidedServicesDefault}
                                                    />
                                                </Form.Group>


                                                <h6 className="mt-5">Notification settings</h6>
                                                <hr />
                                                <Form.Group>
                                                    <Form.Check
                                                        custom
                                                        type="checkbox"
                                                        id="checkbox4"
                                                        name="editReceiveSmsN"
                                                        label="Receive SMS notifications on appointment changes"
                                                        onChange={this.checkBoxChange}
                                                        checked={this.state.editReceiveSmsN}
                                                    />
                                                    <Form.Check
                                                        custom
                                                        type="checkbox"
                                                        id="checkbox5"
                                                        name="editReceiveEmailN"
                                                        label="Receive email notifications on appointment changes"
                                                        onChange={this.checkBoxChange}
                                                        checked={this.state.editReceiveEmailN}
                                                    />
                                                </Form.Group>



                                                <h6 className="mt-5">Locations for this employee</h6>
                                                <hr />
                                                <Form.Group>
                                                    <Form.Check
                                                        custom
                                                        type="checkbox"
                                                        id="checkbox6"
                                                        name="editLocationDefault"
                                                        label="Default"
                                                        onChange={this.checkBoxChange}
                                                        checked={this.state.editLocationDefault}
                                                    />
                                                </Form.Group>
                                            </Form>
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <Button variant="secondary" onClick={this.editStaffModelClose}>
                                                Close
                              </Button>
                                            <Button variant="primary" onClick={this.editStaffSubmit}>
                                                Update Changes
                              </Button>
                                        </Modal.Footer>
                                    </Modal>

                                    <Modal show={this.state.viewStaffModel}>
                                        <Modal.Header >
                                            <Modal.Title>Staff View</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                            <Form>
                                                <Table striped bordered hover size="sm">

                                                    <tr>
                                                        <th>Email</th>
                                                        <td>{this.state.viewStaffEmail}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>First Name</th>
                                                        <td>{this.state.viewStaffFirstName}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Last Name</th>
                                                        <td>{this.state.viewStaffLastName}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Home Phone</th>
                                                        <td>${this.state.viewStaffHomePhone}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Mobile Number</th>
                                                        <td>{this.state.viewMobileNumber}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Rules</th>
                                                        <td><input type="checkbox" checked={this.state.viewServiceProvider} /> <label >Service provider</label><input type="checkbox" checked={this.state.viewScheduler} /> <label >Scheduler</label></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Provided services</th>
                                                        <td><input type="checkbox" checked={this.state.viewProvidedServicesDefault} /> <label >Default</label></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Notification settings</th>
                                                        <td><input type="checkbox" checked={this.state.viewReceiveSmsN} /> <label >SMS notifications</label><input type="checkbox" checked={this.state.viewReceiveEmailN} /> <label >email notifications</label></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Locations for this employee</th>
                                                        <td><input type="checkbox" checked={this.state.viewLocationDefault} /> <label >Default</label></td>
                                                    </tr>
                                                </Table>
                                            </Form>


                                        </Modal.Body>
                                        <Modal.Footer>
                                            <Button variant="secondary" onClick={this.viewStaffModelClose}>
                                                Close
                                        </Button>
                                        </Modal.Footer>
                                    </Modal>
                                </div>
                                <Row>

                                </Row>

                            </Card.Body>
                        </Card>

                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default StaffListing;
