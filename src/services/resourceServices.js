import axios from 'axios';
import {url} from '../config/url'
import decode from 'jwt-decode';
import AuthService from "./authService";

export default class ResourceService extends AuthService {
     constructor() {
          super();
      }
  

   addResource(dataVo){
        return axios.post(`${url.ADD_RESOURCE}`,dataVo,super.setTokenToRequest())
        .then(res => {
         return res.data;
        }).catch(err =>{
         console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
        })
     }

     getResourcesData(ownerId){
          return axios.get(`${url.RESOURCES_LIST + ownerId}`, super.setTokenToRequest())
          .then(res => {
           return res.data;
          }).catch(err =>{
           console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          })
      }

      delResources(id) { 
          return axios.get(url.RESOURCES_DEL + id, super.setTokenToRequest())
          .then(res => {
              return (res.data.data);
          }).catch(err => {
              console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          });
          
        }

        updateResourcInfo(resourceVo) { 
          console.log('resourceVoresourceVo',resourceVo)
          return axios.post(`${url.RESOURCES_UPDATE+resourceVo.id}`,resourceVo, super.setTokenToRequest())
          .then(res => {
           return res.data;
          }).catch(err =>{
           console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          })
        
     }
     
     LoginUserInfo(ownerId){
        return axios.get(`${url.GET_USER_INFO + ownerId}`, super.setTokenToRequest())
        .then(res => {
         return res.data;
        }).catch(err =>{
         console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
        })
    }

    

    





}
