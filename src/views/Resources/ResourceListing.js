import React from 'react';
import {Row, Col, Card, Button, Modal, Form, Alert} from 'react-bootstrap';
import Aux from "../../hoc/_Aux";
import { MDBDataTable } from 'mdbreact';
import 'mdbreact/dist/css/mdb.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import ResourceService from '../../services/resourceServices'
import { confirmAlert } from 'react-confirm-alert'; 
import 'react-confirm-alert/src/react-confirm-alert.css';
const Api = new ResourceService()
let RowArray = [];
class ResourceListing extends React.Component { 

    constructor(props) {
        super(props)
        this.state = {
            lists:[],
            editModel:false
        }
       
        this.handleClose = this.handleClose.bind(this);
    }
    componentDidMount() {
        
            this.getResourcesList();
       
    }
    handleInputChange = event => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
          [name]: value,
          isinvalid: ''
        })
      }
    getResourcesList = () => {
        const ownerId = Api.getProfile().id;
        
        Api.getResourcesData(ownerId)
            .then((result) => {
                RowArray = [];
                for(let i=0; i<result.data.length; i++){
                      RowArray.push({
                        resourceName:result.data[i].resourceName, 
                        quantity:result.data[i].quantity,
                        description:result.data[i].description,
                       
                           action:[
                           <span
                           className='iconSet' id="editResource"  onClick={() => this.editResource(result.data[i])}
                           
                         >
                         <i className="feather icon-edit"></i>
                         
                         </span>, <span  className='iconSet' id="delResource" onClick={() => this.submitCorfirm(result.data[i]._id)} > 
                         <i className="feather icon-trash-2"></i> 
                         
                         </span>]
                      });
              }
              RowArray = RowArray.reverse()
              this.setState({ RowArray: RowArray });
                
            }).catch(err => {
                console.log('xxx', err);
            })
    }

    submitCorfirm = (id) => {
        confirmAlert({
          title: 'Confirm to Delete Resources',
          message: 'Are you sure to do this.',
          buttons: [
            {
              label: 'Yes',
             onClick: () => this.delResources(id)
            },
            {
              label: 'No',
            }
          ]
        });
      };

      delResources=(id)=>{
        Api.delResources(id)
        .then((result) => {
          this.getResourcesList(); 
       }).catch(err => {
           console.log('xxx', err);
       }) 
      }

      editResource=(resourceVo)=>{
          
        this.setState({
          editModel:!this.state.editModel,
          editId:resourceVo._id,
          editResourceName:resourceVo.resourceName,
          editQuantity:resourceVo.quantity,
          editDescription:resourceVo.description,
         
        })
      }

      handleClose=()=>{
        this.setState({
            editModel:!this.state.editModel,
           
          })
      }

      editResourceSubmit = () =>{
        const resourceVo={
          id:this.state.editId,
          ownerId: Api.getProfile().id,
          resourceName:this.state.editResourceName,
          quantity:this.state.editQuantity,
          description:this.state.editDescription,
         
        }
        
        Api.updateResourcInfo(resourceVo)
        .then(result=>{
       if(result.success){
         this.getResourcesList();
         this.setState({
          editModel:!this.state.editModel,
         })
       }
        }).catch(err=>{
          console.log('err',err)
        })
      }

    render() {
        const data = {
            columns: [
                {
                    label: 'Resource name',
                    field: 'resourceName',
                    sort: 'asc',
                    width: 150
                },
                  {
                    label: 'Resource quantity',
                    field: 'quantity',
                    sort: 'asc',
                    width: 150
                  },
                  {
                    label: 'Description',
                    field: 'description',
                    sort: 'asc',
                    width: 150
                  },
                 
                 
                  {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 150
                  },
            ],
            rows: RowArray
        }
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">Resource Listing</Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <div>
                                <MDBDataTable
                                    striped
                                    data={data}
                                    hover
                                />
                                <Modal show={this.state.editModel}>
        <Modal.Header >
          <Modal.Title>Resource update</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Name</Form.Label>
                                                <Form.Control type="text" placeholder="Enter name" name="editResourceName" value={this.state.editResourceName}  onChange={this.handleInputChange} />
                                               
                                            </Form.Group>

                                            <Form.Group controlId="formBasicName">
                                                <Form.Label>Quantity</Form.Label>
                                                <Form.Control type="number" placeholder="Quantity" name="editQuantity" onChange={this.handleInputChange} value={this.state.editQuantity} />
                                               
                                                </Form.Group>

                                            <Form.Group controlId="exampleForm.ControlTextarea1">
                                            <Form.Label>Description</Form.Label>
                                            <Form.Control as="textarea" rows="3" name="editDescription" onChange={this.handleInputChange} value={this.state.editDescription} />
                                           
                                            </Form.Group>
                                           
                                        </Form>
                                    
                                            <Form.Group controlId="formBasicFirstName">
                                           
                                         

                                            {this.state.alertBox ? (
                                                <Alert variant={this.state.actionStatus}>
                                                  {this.state.actionMessage}
                                                </Alert>
                                              ) : (
                                                ''
                                              )}
                                      
                                           
                                            </Form.Group>
                                      
   
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={this.editResourceSubmit}>
            Update Changes
          </Button>
        </Modal.Footer>
      </Modal>

                            </div>
                              
                              
                            </Card.Body>
                        </Card>
                       
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default ResourceListing;
