import React from 'react';

const Business = React.lazy(() => import('./Demo/Authentication/SignUp/SignUp1'));
const Signin1 = React.lazy(() => import('./Demo/Authentication/SignIn/SignIn1'));


const route = [
    { path: '/auth/business', exact: true, name: 'Business', component: Business },
    { path: '/auth/signin-1', exact: true, name: 'Signin 1', component: Signin1 },
    
];

export default route;