import axios from 'axios';
import {url} from '../config/url'
import AuthService from "./authService";

export default class StaffService extends AuthService {
     constructor() {
          super();
      }
  

   addStaff(staffVo){
        return axios.post(`${url.ADD_STAFF}`,staffVo,super.setTokenToRequest())
        .then(res => {
         return res.data;
        }).catch(err =>{
         console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
        })
     } 

     getStaffList(ownerId){
          return axios.get(`${url.LIST_STAFF + ownerId}`,super.setTokenToRequest())
          .then(res => {
           return res.data;
          }).catch(err =>{
           console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          })
       } 

       updateStaffInfo(staffVo){
          return axios.post(`${url.STAFF_UPDATE + staffVo.id}`,staffVo,super.setTokenToRequest())
          .then(res => {
           return res.data;
          }).catch(err =>{
           console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          })
       } 
       deleteStaff(staffVo){
          return axios.post(`${url.STAFF_DELETE + staffVo.id}`,staffVo,super.setTokenToRequest())
          .then(res => {
           return res.data;
          }).catch(err =>{
           console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          })
       }

  

}
