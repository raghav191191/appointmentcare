import React from 'react';
import $ from 'jquery';

window.jQuery = $;
window.$ = $;
global.jQuery = $;

const DashboardDefault = React.lazy(() => import('./Demo/Dashboard/Default'));
const Dashboard = React.lazy(() => import('./views/user-site/UserDashboard/UserDashboard'));




const routes = [
    // { path: '/dashboard/default', exact: true, name: 'Default', component: DashboardDefault },
    { path: '/business/dashboard', exact: true, name: 'Default', component: Dashboard },
];

export default routes;