import React from 'react';
import {Row, Col, Card, Form, Button, Alert} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";
import ResourceService from '../../services/resourceServices'

const Api = new ResourceService()

class AddResource extends React.Component { 
    constructor (props) {
        super(props)
        this.state = {
          errors: {},
          alertBox: false,
          actionMessage: '',
          actionStatus: '',
        }
      }
    
    componentDidMount () {}

    handleInputChange = event => {
      const target = event.target
      const value = target.value
      const name = target.name
      this.setState({
        [name]: value,
        isinvalid: ''
      })
    }
    handleValidation () {
        let errors = {}
        let formIsValid = true
        // Name
        if (!this.state.resourceName) {
          formIsValid = false
          errors['resourceName'] = 'Cannot be empty'
        }
        // Email
        if (!this.state.quantity) {
            formIsValid = false
            errors['quantity'] = 'Cannot be empty'
          }
       
        if (!this.state.description) {
          formIsValid = false
          errors['description'] = 'Cannot be empty'
        }
       
        this.setState({ errors: errors })
        return formIsValid
      }
    AddResource=()=>{
        const dataVo = {
            ownerId:Api.getProfile().id, //tokenId
            resourceName: this.state.resourceName,
            quantity: this.state.quantity,
            description: this.state.description,
            
        }

      
        if(this.handleValidation()){
            Api.addResource(dataVo)
        .then(result => {
            console.log('xxxx result', result);
            if (result.success) {
                this.setState({
                  actionMessage: result.message,
                  actionStatus: 'success',
                  alertBox: true,
                })
                
                setTimeout(() => {
                  this.setState({
                    alertBox: false
                  })
                }, 2000)
               this.props.history.replace('/ResourceListing');
              } else {
                this.setState({
                  actionMessage: result.message,
                  actionStatus: 'warning',
                  alertBox: false,
                })
              }
            
           
        })
        .catch(err => {
          console.log('err', err)
        })
        }
        
        

    }

    render() {

        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">Create a new Resource</Card.Title>
                            </Card.Header>
                            <Card.Body>
                          <h6>Resource Information</h6>
                            <Row>
                                    <Col md={6}>
                                        <Form>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Name</Form.Label>
                                                <Form.Control type="text" placeholder="Enter name" name="resourceName"  onChange={this.handleInputChange} />
                                                <span className='error-msg'>
                                                {this.state.errors.resourceName}
                                              </span>
                                            </Form.Group>

                                            <Form.Group controlId="formBasicName">
                                                <Form.Label>Quantity</Form.Label>
                                                <Form.Control type="number" placeholder="Quantity" name="quantity" onChange={this.handleInputChange} />
                                                <span className='error-msg'>
                                                {this.state.errors.quantity}
                                              </span>
                                                </Form.Group>

                                            <Form.Group controlId="exampleForm.ControlTextarea1">
                                            <Form.Label>Description</Form.Label>
                                            <Form.Control as="textarea" rows="3" name="description" onChange={this.handleInputChange} />
                                            <span className='error-msg'>
                                            {this.state.errors.description}
                                          </span>
                                            </Form.Group>
                                           
                                        </Form>
                                    
                                            <Form.Group controlId="formBasicFirstName">
                                           
                                            <Button variant="primary" onClick={this.AddResource}>
                                            Create Resource
                                            </Button>
                                            <Button variant="danger">
                                            Cancle
                                            </Button>

                                            {this.state.alertBox ? (
                                                <Alert variant={this.state.actionStatus}>
                                                  {this.state.actionMessage}
                                                </Alert>
                                              ) : (
                                                ''
                                              )}
                                      
                                           
                                            </Form.Group>
                                      
                                      
                                    </Col>
                                </Row>
                              
                            </Card.Body>
                        </Card>
                       
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default AddResource;
