import axios from 'axios';
import {url} from '../config/url'
import decode from 'jwt-decode';
import AuthService from "./authService";

export default class SiteService extends AuthService {
     constructor() {
          super();
      }
  

   getDirectoryList(){
        return axios.get(`${url.DIRECTORYLIST}`,super.setTokenToRequest())
        .then(res => {
         return res.data;
        }).catch(err =>{
         console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
        })
     }
}
