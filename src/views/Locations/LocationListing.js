import React from 'react';
import { Row, Col, Card, Form, Modal, Button } from 'react-bootstrap';

import Aux from "../../hoc/_Aux";
import { MDBDataTable } from 'mdbreact';
import 'mdbreact/dist/css/mdb.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import LocationService from '../../services/locationService'
import ResourceServices from '../../services/resourceServices'
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
const Api = new LocationService()
const RApi = new ResourceServices()
let RowArray = [];

class LocationListing extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      lists: [],
      editModel: false,
      employeeLocation: false,
      locationName: '',
      locationAddress1: '',
      locationAddress2: '',
      locationCity: '',
      locationState: '',
      locationZipcode: '',
      phoneNumber: '',
      mobileNumber: '',
      fax: '',
      employeeLocation: ''
    }

    this.handleClose = this.handleClose.bind(this);
  }
  componentDidMount() {

    this.getLocationList();
    this.getLoginUserInfo();

  }
  getLoginUserInfo = () => {
    const ownerId = Api.getProfile().id;

    RApi.LoginUserInfo(ownerId)
      .then((result) => {
        this.setState({
          ownerFirstName: result.data[0].ownerFirstName,
          ownerLastName: result.data[0].ownerLastName
        })
      }).catch(err => {
        console.log('xxx', err);
      })

  }
  getInputTextValue = event => {
    const target = event.target
    const value = target.value
    const name = target.name
    this.setState({
      [name]: value,
      isinvalid: ''
    })
  }
  getLocationList = () => {
    const ownerId = Api.getProfile().id;

    Api.getLocationData(ownerId)
      .then((result) => {
        console.log('xxxxx-xxx-xxx', result);

       

        RowArray = [];
        for (let i = 0; i < result.data.length; i++) {
          
          RowArray.push({
            locationName: result.data[i].locationName,
            fullAdress: result.data[i].locationAddress1 + ', ' + result.data[i].locationAddress2 + ', ' + result.data[i].locationCity + ', ' + result.data[i].locationState + ', ' + result.data[i].locationZipcode,
            phoneNumber: result.data[i].phoneNumber,
            mobileNumber: result.data[i].mobileNumber,
            fax: result.data[i].fax,

            action: [
              <span
                className='iconSet' id="editResource" onClick={() => this.editResource(result.data[i])}

              >
                <i className="feather icon-edit"></i>

              </span>, <span className='iconSet' id="delResource" onClick={() => this.submitCorfirm(result.data[i]._id)} >
                <i className="feather icon-trash-2"></i>

              </span>]
          });
        }
        RowArray = RowArray.reverse()
        this.setState({ RowArray: RowArray });

      }).catch(err => {
        console.log('xxx', err);
      })
  }
  submitCorfirm = (id) => {
    confirmAlert({
      title: 'Confirm to Delete Location',
      message: 'Are you sure to do this.',
      buttons: [
        {
          label: 'Yes',
          onClick: () => this.delLocationData(id)
        },
        {
          label: 'No',
        }
      ]
    });
  };

  delLocationData = (id) => {
    Api.delLocation(id)
      .then((result) => {
        this.getLocationList();
      }).catch(err => {
        console.log('xxx', err);
      })
  }

  editResource = (dataVo) => {
    this.setState({
      editModel: !this.state.editModel,
      editId: dataVo._id,
      locationName: dataVo.locationName,
      locationAddress1: dataVo.locationAddress1,
      locationAddress2: dataVo.locationAddress2,
      locationCity: dataVo.locationCity,
      locationState: dataVo.locationState,
      locationZipcode: dataVo.locationZipcode,
      phoneNumber: dataVo.phoneNumber,
      mobileNumber: dataVo.mobileNumber,
      fax: dataVo.fax,
      employeeLocation: dataVo.employeeLocation
    })
  }
  handleClose = () => {
    this.setState({
      editModel: !this.state.editModel,
    })
  }

  editLocationSubmitBtn = () => {
    const dataVo = {
      id: this.state.editId,
      ownerId: Api.getProfile().id,
      locationName: this.state.locationName,
      locationAddress1: this.state.locationAddress1,
      locationAddress2: this.state.locationAddress2,
      locationCity: this.state.locationCity,
      locationState: this.state.locationState,
      locationZipcode: this.state.locationZipcode,
      phoneNumber: this.state.phoneNumber,
      mobileNumber: this.state.mobileNumber,
      fax: this.state.fax,
      employeeLocation: this.state.employeeLocation

    }

    Api.updateLocationInfoVo(dataVo)
      .then(result => {
        if (result.success) {
          this.getLocationList();
          this.setState({
            editModel: !this.state.editModel,
          })
        }
      }).catch(err => {
        console.log('err', err)
      })
  }

  handleCheckboxChange = () => {

    this.setState({
      employeeLocation: !this.state.employeeLocation
    })
  }



  render() {
    const data = {
      columns: [
        {
          label: 'Name',
          field: 'locationName',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Full Address',
          field: 'fullAdress',
          sort: 'asc',
          width: 150
        },
        {
          label: ' Phone Number',
          field: 'phoneNumber',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Mobile Number',
          field: 'mobileNumber',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Fax',
          field: 'fax',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Action',
          field: 'action',
          sort: 'asc',
          width: 150
        },
      ],
      rows: RowArray
    }

    return (
      <Aux>
        <Row>
          <Col>
            <Card>
              <Card.Header>
                <Card.Title as="h5">Locations</Card.Title>
                <span>View and manage the locations offered by your business.</span>
              </Card.Header>
              <Card.Body>

                <div>
                  <MDBDataTable
                    striped
                    data={data}
                    hover
                  />
                  <Modal show={this.state.editModel}>
                    <Modal.Header >
                      <Modal.Title>Location update</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Form>
                        <Form.Group controlId="locationName">
                          <Form.Label>Name</Form.Label>
                          <Form.Control type="text" placeholder="Enter name"
                            name='locationName' value={this.state.locationName}
                            onChange={this.getInputTextValue}
                          />

                        </Form.Group>

                        <Form.Group controlId="locationAddress1">
                          <Form.Label>Address1</Form.Label>
                          <Form.Control type="text" placeholder="Address1" value={this.state.locationAddress1}
                            name='locationAddress1'
                            onChange={this.getInputTextValue}
                          />
                        </Form.Group>

                        <Form.Group controlId="locationAddress2">
                          <Form.Label>Address2</Form.Label>
                          <Form.Control type="text" placeholder="Address2"
                            name='locationAddress2' value={this.state.locationAddress2}
                            onChange={this.getInputTextValue}
                          />
                        </Form.Group>
                        <Form.Group controlId="locationCity">
                          <Form.Label>City</Form.Label>
                          <Form.Control type="text" placeholder="City"
                            name='locationCity' value={this.state.locationCity}
                            onChange={this.getInputTextValue}
                          />
                        </Form.Group>
                        <Form.Group controlId="locationState">
                          <Form.Label>State</Form.Label>
                          <Form.Control type="text" placeholder="State"
                            name='locationState' value={this.state.locationState}
                            onChange={this.getInputTextValue}
                          />
                        </Form.Group>
                        <Form.Group controlId="locationZipcode">
                          <Form.Label>ZipCode</Form.Label>
                          <Form.Control type="text" placeholder="Zip Code"
                            name='locationZipcode' value={this.state.locationZipcode}
                            onChange={this.getInputTextValue}
                          />
                        </Form.Group>
                        <Form.Group controlId="phoneNumber">
                          <Form.Label>Phone Number</Form.Label>
                          <Form.Control type="text" placeholder="Phone number"
                            name='phoneNumber' value={this.state.phoneNumber}
                            onChange={this.getInputTextValue}
                          />
                        </Form.Group>
                        <Form.Group controlId="mobileNumber">
                          <Form.Label>Mobile Number</Form.Label>
                          <Form.Control type="text" placeholder="State"
                            name='mobileNumber' value={this.state.mobileNumber}
                            onChange={this.getInputTextValue}
                          />
                        </Form.Group>
                        <Form.Group controlId="fax">
                          <Form.Label>Fax</Form.Label>
                          <Form.Control type="text" placeholder="Fax"
                            name='fax' value={this.state.fax}
                            onChange={this.getInputTextValue}
                          />
                        </Form.Group>

                        <Col md={12}>
                          <h6 className="mt-5">Employees in location</h6>
                          <hr />
                          <Form.Group>
                            <Form.Check
                              custom
                              type="checkbox"
                              id="checkbox1"
                              name="employeeLocation"
                              label={this.state.ownerFirstName + ' ' + this.state.ownerLastName}
                              onChange={this.handleCheckboxChange}
                              checked={this.state.employeeLocation}
                            />
                          </Form.Group>
                        </Col>
                      </Form>



                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="secondary" onClick={this.handleClose}>
                        Close
                              </Button>
                      <Button variant="primary" onClick={this.editLocationSubmitBtn}>
                        Update Changes
                              </Button>
                    </Modal.Footer>
                  </Modal>

                </div>

              </Card.Body>
            </Card>

          </Col>
        </Row>
      </Aux>
    );
  }
}

export default LocationListing;
