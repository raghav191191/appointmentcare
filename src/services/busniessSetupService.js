import axios from 'axios';
import {url} from '../config/url';
import AuthService from "./authService";

export default class BusniessService extends AuthService {
    constructor() {
        super();
    }

    
    updateBusniessInfo(dataVo) { 
        return axios.post(`${url.UPDATE_BUSNIESS}`,dataVo, super.setTokenToRequest())
        .then(res => {
         return res.data;
        }).catch(err =>{
         console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
        })
   }

   getBusniessDetails(id) { 
    return axios.get(url.GET_DETAILS_BUSNIESS + id, super.setTokenToRequest()) 
    .then(res => {
        return (res.data);
    }).catch(err => {
        console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
    });
    
  }
    

    

  
}