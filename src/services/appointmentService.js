import axios from 'axios';
import {url} from '../config/url'
import decode from 'jwt-decode';
import AuthService from "./authService";

export default class AppointmentService extends AuthService {
     constructor() {
          super();
      }
  

      addAppointment(dataVo){
        return axios.post(`${url.ADD_APPOINTMENT}`,dataVo,super.setTokenToRequest())
        .then(res => {
         return res.data;
        }).catch(err =>{
         console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
        })
     }

     getAppointmentData(ownerId){
          return axios.get(`${url.APPOINTMENT_LIST + ownerId}`, super.setTokenToRequest())
          .then(res => {
           return res.data;
          }).catch(err =>{
           console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          })
      }

      delAppointment(id) { 
          return axios.get(url.APPOINTMENT_DEL + id, super.setTokenToRequest())
          .then(res => {
              return (res.data.data);
          }).catch(err => {
              console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          });
          
     }
     updateAppointmentInfoVo(dataVo) { 
          return axios.post(`${url.APPOINTMENT_UPDATE+dataVo.id}`,dataVo, super.setTokenToRequest())
          .then(res => {
           return res.data;
          }).catch(err =>{
           console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
          })
        
     }

     addBreak(dataVo){
        return axios.post(`${url.ADD_BREAK}`,dataVo,super.setTokenToRequest())
        .then(res => {
         return res.data;
        }).catch(err =>{
         console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
        })
     }

     
     getBreakPointData(ownerId){
        return axios.get(`${url.BREAK_LIST + ownerId}`, super.setTokenToRequest())
        .then(res => {
         return res.data;
        }).catch(err =>{
         console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
        })
    }

    updateBreakInfoVo(dataVo) { 
        return axios.post(`${url.BREAK_UPDATE+dataVo.id}`,dataVo, super.setTokenToRequest())
        .then(res => {
         return res.data;
        }).catch(err =>{
         console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
        })
      
   }

   delBreakPeriod(id) { 
    return axios.get(url.BREAK_DEL + id, super.setTokenToRequest())
    .then(res => {
        return (res.data.data);
    }).catch(err => {
        console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
    });
    
}

editAppointmentData(dataVo) { 
    return axios.post(`${url.APPOINTMENT_UPDATE_BY_ID+dataVo.id}`,dataVo, super.setTokenToRequest())
    .then(res => {
     return res.data;
    }).catch(err =>{
     console.log('xxxxxxxxx xxxxxxxxxxxxx error ' + err);
    })
  
}

}
