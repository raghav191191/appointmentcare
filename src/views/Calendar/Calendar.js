import React from 'react';
import { Row, Col, Card, Modal, Button, Form, Tabs, Tab } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import TimePicker from 'react-bootstrap-time-picker';
import DatePicker from 'react-datepicker'

import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from '@fullcalendar/timegrid'
import listPlugin from '@fullcalendar/list';
import interactionPlugin from "@fullcalendar/interaction";
import Aux from "../../hoc/_Aux";
import "react-datepicker/dist/react-datepicker.css";
import AppointmentService from '../../services/appointmentService'
import LocationService from '../../services/locationService'
import ServicesServices from '../../services/serviceServices'
import CustomerService from '../../services/customerService'
import StaffService from '../../services/staffService'

const Api = new AppointmentService()
const Lo_Api = new LocationService()
const Se_Api = new ServicesServices()
const Cu_Api = new CustomerService()
const St_Api = new StaffService()

class CalendarView extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            locationLists: [],
            serviceLists: [],
            customerLists: [],
            appointmentLists:[],
            staffLists:[],
            BreakList:[],
            editModel: false,
            smsConfirmation: false,
            appoinmentCourse: false,
            startDate: new Date(),
            time: 0,
            appoinmentCourse:false,
            breakModel: false,
            viewModel: false,
        }

        this.handleClose = this.handleClose.bind(this);
        this.handleViewClose = this.handleViewClose.bind(this);
        this.handleBreakClose = this.handleBreakClose.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
    }

    componentDidMount() {
        this.getLocationList();
        this.getServicesList();
        this.getCustomerList();
        this.getStaffList();

    }

    addDays=()=>{
        var date = new Date()
        date.setDate(date.getDate() + 1);
        return date;
      }

    formatDate = date => {
        const eventDate = new Date(date)
        const monthIndex = ('0' + (eventDate.getMonth() + 1)).slice(-2)
        const day = ('0' + eventDate.getDate()).slice(-2)
        const year = eventDate.getFullYear()
        return year + '-' + monthIndex + '-' + day
      }

    formatDate01 = (date,from) => {
        const eventDate = new Date(date)
        //return eventDate
        const monthIndex = ('0' + (eventDate.getMonth() + 1)).slice(-2)
        const day = ('0' + (eventDate.getDate())).slice(-2)
        const year = eventDate.getFullYear()
        let findal=year + '-' + monthIndex + '-' + day
        findal= new Date(findal+' '+from)
        return findal
      }
      formatDate02 = (date,to,Duration) => {
        const eventDate = new Date(date)
        const monthIndex = ('0' + (eventDate.getMonth() + 1)).slice(-2)
        const day = ('0' + eventDate.getDate()).slice(-2)
        const year = eventDate.getFullYear()
        let findal=year + '-' + monthIndex + '-' + day
    
        findal= new Date(findal+' '+to)
        findal.setMinutes ( findal.getMinutes() + Duration );
      
        
        return findal
      }
      getBreakPoint=()=>{
        const ownerId = Lo_Api.getProfile().id;
        Api.getBreakPointData(ownerId)
            .then((result) => {
                console.log('xxxx result zxx', result);
                
                result.data.map(res => {
                    res['title'] = res.title+' - '+ this.getStaffById(res.serviceProvider)
                    res['start'] = this.formatDate01(res.appointmentDate,res.appointmentTime)
                    res['end'] = this.formatDate02(res.appointmentDate, res.appointmentTime,res.Duration)
                    res['backgroundColor'] ='red'
                    res['borderColor'] ='red'
                  })
              
                this.setState({
                    BreakList:result.data
                   })
                   this.getAppointmentList()
              
            }).catch(err => {
                console.log('xxx', err);
            })

      }
    getAppointmentList=()=>{
        const ownerId = Lo_Api.getProfile().id;
        Api.getAppointmentData(ownerId)
            .then((result) => {
                result.data.map(res => {
                     res['title'] = this.getStaffById(res.serviceProvider)+' - '+this.getCustomerById(res.CustomerId)
                     res['start'] = this.formatDate01(res.appointmentDate,res.appointmentTime)
                     res['end'] = this.formatDate02(res.appointmentDate, res.appointmentTime,res.Duration)
                     res['backgroundColor'] ='green'
                     res['borderColor'] ='green'
                   })
                   const aa3=[...result.data, ...this.state.BreakList]
                   this.setState({
                    appointmentLists:aa3,
                   })
              
            }).catch(err => {
                console.log('xxx', err);
            })
    }
   
    getStaffList = () => {
        const ownerId = Lo_Api.getProfile().id;
        St_Api.getStaffList(ownerId)
            .then((result) => {
                this.setState({
                    staffLists: result.data
                })
               
                this.getBreakPoint();
            }).catch(err => {
                console.log('xxx', err);
            })
    }
    getStaffById = valueId => {
        if (valueId) {
          let nameData = ''
          for (let i = 0; i < this.state.staffLists.length; i++) {
            if (valueId == this.state.staffLists[i]._id) {
              nameData =
              this.state.staffLists[i].firstName + ' ' + this.state.staffLists[i].lastName 
            }
          }
          return nameData
        }
      }
    getLocationList = () => {
        const ownerId = Lo_Api.getProfile().id;
        Lo_Api.getLocationData(ownerId)
            .then((result) => {
                this.setState({
                    locationLists: result.data
                })
            }).catch(err => {
                console.log('xxx', err);
            })
    }
    getServicesList = () => {
        const ownerId = Se_Api.getProfile().id;
        Se_Api.getServicesData(ownerId)
            .then((result) => {

                this.setState({
                    serviceLists: result.data
                })

            }).catch(err => {
                console.log('xxx', err);
            })
    }
    getCustomerList = () => {
        const ownerId = Cu_Api.getProfile().id;
        Cu_Api.getCustomerData(ownerId)
            .then((result) => {
                this.setState({
                    customerLists: result.data
                })

            }).catch(err => {
                console.log('xxx', err);
            })
    }
    getCustomerById = valueId => {
        if (valueId) {
          let nameData = ''
          for (let i = 0; i < this.state.customerLists.length; i++) {
            if (valueId == this.state.customerLists[i]._id) {
              nameData =
              this.state.customerLists[i].customerFirstName + ' ' + this.state.customerLists[i].custmerLastName 
            }
          }
          return nameData
        }
      }
    handleChange = date => {
        this.setState({
            startDate: date
        });
    };

    convertSecondstoTime=(given_seconds)=> { 
        let timeString=''
       let dateObj = new Date(given_seconds * 1000); 
       let hours = dateObj.getUTCHours(); 
       let minutes = dateObj.getUTCMinutes(); 
        let seconds = dateObj.getSeconds(); 

       return timeString = hours.toString().padStart(2, '0') 
            + ':' + minutes.toString().padStart(2, '0') 
           ; 
    } 

    handleTimeChange(time) {
        let AssignFrom=this.convertSecondstoTime(time)
        this.setState({ time:AssignFrom });
    }

    handleDateClick = (arg) => { // bind with an arrow function
       const myDate= new Date(arg.dateStr)
        let selectTime=  ('0' + myDate.getHours()).slice(-2)+':'+       ('0' + myDate.getMinutes()).slice(-2)
        this.setState({
            editModel: true,
            startDate:new Date(arg.dateStr),
            time: selectTime
        })

    }
    handleClose = () => {
        this.setState({
            editModel: !this.state.editModel,
        })
    }
    handleViewClose=()=>{
        this.setState({
            viewModel: !this.state.viewModel,
        })
    }

    handleBreakClose=()=>{
        this.setState({
            breakModel: !this.state.breakModel,
        })
    }

    getInputTextValue = event => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value,
            isinvalid: '',
        })
    }
    checkCustomerCheckBox = () => {
        this.setState({
            appoinmentCourse: !this.state.appoinmentCourse
        })
    }
    checkBoxChange=()=>{
        this.setState({
            smsConfirmation: !this.state.smsConfirmation
        })
    }
    addBreakPeriod=()=>{
        const dataVo={
           
            ownerId:Api.getProfile().id, //tokenId
            title:"Break",
            appointmentDate:this.formatDate(this.state.startDate),
            appointmentTime:this.state.time,
            serviceProvider:this.state.serviceProvider,
            location:this.state.location,
            Duration:this.state.Duration,
            appointmentNotes:this.state.appointmentNotes,
            
        }

        Api.addBreak(dataVo)
        .then(result => {
                this.setState({
                  editModel: false,
                })
                this.getBreakPoint()
                this.getAppointmentList()
        })
        .catch(err => {
          console.log('err', err)
        })
        

    }
    updateBreakPeriod=()=>{
        const dataVo={
            id:this.state.breakId,
            ownerId:Api.getProfile().id, //tokenId
            appointmentTime:this.state.time,
            serviceProvider:this.state.serviceProvider,
            location:this.state.location,
            Duration:this.state.Duration,
            appointmentNotes:this.state.appointmentNotes,
           
        }
        Api.updateBreakInfoVo(dataVo)
        .then(result => {
          if (result.success) {
           
            this.getBreakPoint()
            this.setState({
                breakModel: !this.state.breakModel,
            })
          }
        }).catch(err => {
          console.log('err', err)
        })
        

    }

    editAppointmentSubmit=()=>{
        const dataVo={
            ownerId:Api.getProfile().id, //tokenId
            id:this.state.editId,
            appointmentDate:this.formatDate(this.state.startDate),
            appointmentTime:this.state.time,
            serviceProvider:this.state.serviceProvider,
            location:this.state.location,
            Duration:this.state.Duration,
            appointmentNotes:this.state.appointmentNotes,
            smsConfirmation:this.state.smsConfirmation
        }
        console.log('xxxxx-xx-x-x-x dataVo', dataVo);
        Api.editAppointmentData(dataVo)
        .then(result => {
                this.setState({
                  viewModel: false,
                })
              
                this.getAppointmentList()
        })
        .catch(err => {
          console.log('err', err)
        })
        
    }

    addAppointment=()=>{
       
        const dataVo={
            ownerId:Api.getProfile().id, //tokenId
            appointmentDate:this.formatDate(this.state.startDate),
            appointmentTime:this.state.time,
            serviceProvider:this.state.serviceProvider,
            location:this.state.location,
            Service:this.state.Service,
            Duration:this.state.Duration,
            appointmentNotes:this.state.appointmentNotes,
            appoinmentCourse:this.state.appoinmentCourse,
            appointmentNameCourse:this.state.appointmentNameCourse,
            limitedParticipants:this.state.limitedParticipants,
            CustomerId:this.state.CustomerId,
            smsConfirmation:this.state.smsConfirmation
        }
        Api.addAppointment(dataVo)
        .then(result => {
                this.setState({
                  editModel: false,
                })
              
                this.getAppointmentList()
        })
        .catch(err => {
          console.log('err', err)
        })
    }
    handleDateEditClick = arg => {
       
       console.log('xxxxxxxx arg',  arg.event._def); 
       let appointmentDate=new Date(arg.event._def.extendedProps.appointmentDate);
       let Time=arg.event._def.extendedProps.appointmentTime;
       let serviceProvider=arg.event._def.extendedProps.serviceProvider;
       let location=arg.event._def.extendedProps.location;
       let Duration=arg.event._def.extendedProps.Duration;
       let appointmentNotes=arg.event._def.extendedProps.appointmentNotes;
       let smsConfirmation=arg.event._def.extendedProps.smsConfirmation;
       let Id=arg.event._def.extendedProps._id;

       console.log('xxx-xx-xx-xx breakPoint', arg.event._def.extendedProps.breakPoint);
       

       if(arg.event._def.extendedProps.breakPoint=='breakPoint'){
        this.setState({
            viewModel: false,
            breakModel:true,
            time:Time,
            serviceProvider:serviceProvider,
            location:location,
            Duration:Duration,
            appointmentNotes:appointmentNotes,
            breakId:Id
          })

       }else{
        this.setState({
            viewModel: true,
            breakModel:false,
            startDate:appointmentDate,
            time:Time,
            serviceProvider:serviceProvider,
            location:location,
            Duration:Duration,
            appointmentNotes:appointmentNotes,
            smsConfirmation:smsConfirmation,
            editId:Id,

          
          })

       }
      
        
      }
    
    
      delBreakData = (id) => {
          console.log('xxxxxxxxx del',id);
          
        Api.delBreakPeriod(id)
          .then((result) => {

            this.setState({
                breakModel:false
              })
            this.getBreakPoint();
          }).catch(err => {
            console.log('xxx', err);
          })
      }

      delAppointmentData = (id) => {
        console.log('xxxxxxxxx del xx',id);
        
      Api.delAppointment(id)
        .then((result) => {

          this.setState({
              viewModel:false
            })
          this.getAppointmentList();
        }).catch(err => {
          console.log('xxx', err);
        })
    }
    render() {
       
        
        const header = {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
        }

        const validRange= {
            start: new Date()
          }


        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">Calendar</Card.Title>
                            </Card.Header>
                            <Card.Body>

                                <Row>

                                    <FullCalendar
                                        defaultView='timeGridWeek'
                                        validRange= {validRange}
                                        plugins={[dayGridPlugin, timeGridPlugin, listPlugin, interactionPlugin]}
                                        header={header}
                                        eventClick={this.handleDateEditClick}
                                        dateClick={this.handleDateClick}
                                        events={this.state.appointmentLists}
                                       
                                    />
                                    <Modal show={this.state.editModel} size="lg"
                                        aria-labelledby="contained-modal-title-vcenter"
                                    >
                                        <Modal.Header >
                                            <Modal.Title>event to</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                            <Tabs defaultActiveKey="appointment">

                                                <Tab eventKey="appointment" title="Appointment">


                                                    <Form>
                                                        <Row>
                                                            <Col md={6}>
                                                                <Form>
                                                                    <Form.Group controlId="appointmentDate">
                                                                        <Form.Label>Appointment Date</Form.Label>
                                                                        <DatePicker className="form-control"
                                                                            selected={this.state.startDate}
                                                                            onChange={this.handleChange}
                                                                            minDate={this.addDays()}
                                                                        />
                                                                    </Form.Group>

                                                                    <Form.Group controlId="appointmentTime">
                                                                        <Form.Label>Appointment Time</Form.Label>

                                                                        <TimePicker format={24} start="00:30" end="23:00" step={30} onChange={this.handleTimeChange} value={this.state.time}   />
                                                                    </Form.Group>

                                                                    <Form.Group controlId="serviceProvider">
                                                                        <Form.Label>Service Provider</Form.Label>
                                                                        <Form.Control as="select" name="serviceProvider" onChange={this.getInputTextValue} value={this.state.serviceProvider}>

                                                                            <option value={'*'}>Select provider</option>
                                                                            {this.state.staffLists.map((array, index) =>
                                                                                <option value={array._id}>{array.email}({array.firstName})</option>
                                                                            )}

                                                                        </Form.Control>

                                                                    </Form.Group>
                                                                    <Form.Group controlId="Location">
                                                                        <Form.Label>Location</Form.Label>
                                                                        <Form.Control as="select" name="location" onChange={this.getInputTextValue} value={this.state.location}>
                                                                        <option value={'*'}>Select Location</option>
                                                                            {this.state.locationLists.map((array, index) =>
                                                                                <option value={array._id}>{array.locationName}</option>
                                                                            )}

                                                                        </Form.Control>
                                                                    </Form.Group>
                                                                    <Form.Group controlId="Service">
                                                                        <Form.Label>Service</Form.Label>
                                                                        <Form.Control as="select" name="Service" onChange={this.getInputTextValue} value={this.state.Service}>
                                                                        <option value={'*'}>Select Service</option>
                                                                            {this.state.serviceLists.map((array, index) =>
                                                                                <option value={array._id}>{array.serviceName}</option>
                                                                            )}

                                                                        </Form.Control>
                                                                    </Form.Group>
                                                                    <Form.Group controlId="Duration">
                                                                        <Form.Label>Duration</Form.Label>
                                                                        <Form.Control type="number" placeholder="Duration"
                                                                            name='Duration'
                                                                            onChange={this.getInputTextValue}
                                                                        />
                                                                    </Form.Group>

                                                                </Form>
                                                            </Col>
                                                            <Col md={6}>
                                                                <Form.Group controlId="appointmentNotes">
                                                                    <Form.Label>Appointment Notes</Form.Label>
                                                                    <Form.Control as="textarea" rows="3"
                                                                        name='appointmentNotes'
                                                                        onChange={this.getInputTextValue}
                                                                    />
                                                                </Form.Group>
                                                                <Col md={12}>
                                                                    <h6 className="mt-5">Customer</h6>
                                                                    <hr />
                                                                    <Form.Group>
                                                                        <Form.Check
                                                                            custom
                                                                            type="checkbox"
                                                                            id="checkbox2"
                                                                            name="appoinmentCourse"
                                                                            label="Register this appointment as a course"
                                                                            onChange={this.checkCustomerCheckBox}
                                                                            checked={this.state.appoinmentCourse}
                                                                        />
                                                                    </Form.Group>
                                                                    {this.state.appoinmentCourse ? (
                                                                        <div>
                                                                            <Form.Group controlId="Name">
                                                                                <Form.Label>Name</Form.Label>
                                                                                <Form.Control type="text" placeholder="appointment_name_course"
                                                                                    name='appointmentNameCourse'
                                                                                    onChange={this.getInputTextValue}
                                                                                />
                                                                            </Form.Group>
                                                                            <Form.Group >
                                                                                <Form.Label>Limited participants</Form.Label>
                                                                                <Form.Control type="number" placeholder="Limited participants"
                                                                                    name='limitedParticipants'
                                                                                    onChange={this.getInputTextValue}
                                                                                />
                                                                            </Form.Group>
                                                                        </div>
                                                                    ) : (
                                                                            <Form.Group controlId="formCustomer">
                                                                                <Form.Label>Select Customer</Form.Label>
                                                                                <Form.Control as="select" name="CustomerId" onChange={this.getInputTextValue} value={this.state.formCustomer}>
                                                                                <option value={'*'}>Select Customer</option>
                                                                                    {this.state.customerLists.map((array, index) =>
                                                                                        <option value={array._id}>{array.customerFirstName + ' ' + array.custmerLastName}</option>
                                                                                    )}

                                                                                </Form.Control>
                                                                                <Form.Text className="text-muted">
                                                                                    Please search for a customer, or  <Link to={'/AddCustomer'}> click here to add.</Link>
                                                                                </Form.Text>
                                                                            </Form.Group>
                                                                        )}

                                                                </Col>

                                                                <Col md={12}>
                                                                    <h6 className="mt-5">SMS Confirmation Options</h6>
                                                                    <hr />
                                                                    <Form.Group>
                                                                        <Form.Check
                                                                            custom
                                                                            type="checkbox"
                                                                            id="checkbox1"
                                                                            name="smsConfirmation"
                                                                            label="Send SMS confirmation to customer?"
                                                                            onChange={this.checkBoxChange}
                                                                            checked={this.state.smsConfirmation}
                                                                        />
                                                                    </Form.Group>
                                                                </Col>
                                                                <Form.Group>
                                                                    <Button variant="secondary" onClick={this.handleClose}>
                                                                        Close
                            </Button>
                                                                    <Button variant="primary" onClick={this.addAppointment}>
                                                                        Create Appointment
                            </Button>

                                                                </Form.Group>
                                                            </Col>
                                                        </Row>
                                                    </Form>



                                                </Tab>
                                                <Tab eventKey="break" title="Break">
                                                <Form.Group>
                                                <Form.Label>Break Date</Form.Label>
                                                <DatePicker className="form-control"
                                                    selected={this.state.startDate}
                                                    onChange={this.handleChange}
                                                    minDate={this.addDays()}
                                                />
                                            </Form.Group>

                                            <Form.Group >
                                                <Form.Label>Break start</Form.Label>

                                                <TimePicker format={24} start="00:30" end="23:00" step={30} onChange={this.handleTimeChange} value={this.state.time}   />
                                            </Form.Group>

                                                   
                                                   
                                            <Form.Group>
                                            <Form.Label>Service Provider</Form.Label>
                                            <Form.Control as="select" name="serviceProvider" onChange={this.getInputTextValue} value={this.state.serviceProvider}>

                                                <option value={'*'}>Select provider</option>
                                                {this.state.staffLists.map((array, index) =>
                                                    <option value={array._id}>{array.email}({array.firstName})</option>
                                                )}

                                            </Form.Control>

                                        </Form.Group>
                                        <Form.Group >
                                            <Form.Label>Location</Form.Label>
                                            <Form.Control as="select" name="location" onChange={this.getInputTextValue} value={this.state.location}>
                                            <option value={'*'}>Select Location</option>
                                                {this.state.locationLists.map((array, index) =>
                                                    <option value={array._id}>{array.locationName}</option>
                                                )}

                                            </Form.Control>
                                        </Form.Group>
                                        <Form.Group>
                                        <Form.Label>Duration</Form.Label>
                                        <Form.Control type="number" placeholder="Duration"
                                            name='Duration'
                                            onChange={this.getInputTextValue}
                                        />
                                    </Form.Group>
                                                    <Form.Group controlId="appointmentNotes">
                                                        <Form.Label>Appointment Notes</Form.Label>
                                                        <Form.Control as="textarea" rows="3"
                                                            name='appointmentNotes'
                                                            onChange={this.getInputTextValue}
                                                        />
                                                    </Form.Group>
                                                    <Form.Group controlId="Location">
                                                        <Button variant="secondary" onClick={this.handleClose}>
                                                            Close
           </Button>
                                                        <Button variant="primary" onClick={this.addBreakPeriod}>
                                                            Create Break Period
           </Button>

                                                    </Form.Group>
                                                </Tab>

                                            </Tabs>


                                        </Modal.Body>
                                        <Modal.Footer>

                                        </Modal.Footer>
                                    </Modal>
                                    <Modal show={this.state.viewModel}>
                                    <Modal.Header >
                                      <Modal.Title>Modify Appointment</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                  
                                       
                                         <Form>
                                         <Form.Group controlId="appointmentDate">
                                         <Form.Label>Appointment Date</Form.Label>
                                         <DatePicker className="form-control"
                                             selected={this.state.startDate}
                                             onChange={this.handleChange}
                                             minDate={this.addDays()}
                                         />
                                     </Form.Group>
                                         <Form.Group >
                                             <Form.Label>Appointment start</Form.Label>

                                             <TimePicker format={24} start="00:30" end="23:00" step={30} onChange={this.handleTimeChange} value={this.state.time}   />
                                         </Form.Group>

                                                
                                                
                                         <Form.Group>
                                         <Form.Label>Service Provider</Form.Label>
                                         <Form.Control as="select" name="serviceProvider" onChange={this.getInputTextValue} value={this.state.serviceProvider}>

                                             <option value={'*'}>Select provider</option>
                                             {this.state.staffLists.map((array, index) =>
                                                 <option value={array._id}>{array.email}({array.firstName})</option>
                                             )}

                                         </Form.Control>

                                     </Form.Group>
                                     <Form.Group >
                                         <Form.Label>Location</Form.Label>
                                         <Form.Control as="select" name="location" onChange={this.getInputTextValue} value={this.state.location}>
                                         <option value={'*'}>Select Location</option>
                                             {this.state.locationLists.map((array, index) =>
                                                 <option value={array._id}>{array.locationName}</option>
                                             )}

                                         </Form.Control>
                                     </Form.Group>
                                     <Form.Group>
                                     <Form.Label>Duration</Form.Label>
                                     <Form.Control type="number" placeholder="Duration"
                                         name='Duration'
                                         onChange={this.getInputTextValue}
                                         value={this.state.Duration}
                                     />
                                 </Form.Group>
                                                 <Form.Group >
                                                     <Form.Label>Appointment Notes</Form.Label>
                                                     <Form.Control as="textarea" rows="3"
                                                         name='appointmentNotes'
                                                         onChange={this.getInputTextValue}
                                                         value={this.state.appointmentNotes}
                                                     />
                                                 </Form.Group>
                                                 <Col md={12}>
                                                 <h6 className="mt-5">SMS Confirmation Options</h6>
                                                 <hr />
                                                 <Form.Group>
                                                     <Form.Check
                                                         custom
                                                         type="checkbox"
                                                         id="checkbox1"
                                                         name="smsConfirmation"
                                                         label="Send SMS confirmation to customer?"
                                                         onChange={this.checkBoxChange}
                                                         checked={this.state.smsConfirmation}
                                                     />
                                                 </Form.Group>
                                             </Col>
                                               
                                     </Form>                      
                          
                                                             
                               
                                    </Modal.Body>
                                    <Modal.Footer>
                                     
                                      <Button variant="primary" onClick={this.editAppointmentSubmit}>
                                        Update Appointment
                                      </Button>
                                      <Button variant="danger" onClick={() => this.delAppointmentData(this.state.editId)}  >
                                      Delete
                                    </Button>
                                      <Button variant="secondary" onClick={this.handleViewClose}>
                                      Close
                                    </Button>
                                    </Modal.Footer>
                                  </Modal>
                                  <Modal show={this.state.breakModel}>
                                  <Modal.Header >
                                    <Modal.Title>Modify Break</Modal.Title>
                                  </Modal.Header>
                                  <Modal.Body>
                                
                                        <Form>
                                            <Form.Group >
                                                <Form.Label>Break start</Form.Label>

                                                <TimePicker format={24} start="00:30" end="23:00" step={30} onChange={this.handleTimeChange} value={this.state.time}   />
                                            </Form.Group>

                                                   
                                                   
                                            <Form.Group>
                                            <Form.Label>Service Provider</Form.Label>
                                            <Form.Control as="select" name="serviceProvider" onChange={this.getInputTextValue} value={this.state.serviceProvider}>

                                                <option value={'*'}>Select provider</option>
                                                {this.state.staffLists.map((array, index) =>
                                                    <option value={array._id}>{array.email}({array.firstName})</option>
                                                )}

                                            </Form.Control>

                                        </Form.Group>
                                        <Form.Group >
                                            <Form.Label>Location</Form.Label>
                                            <Form.Control as="select" name="location" onChange={this.getInputTextValue} value={this.state.location}>
                                            <option value={'*'}>Select Location</option>
                                                {this.state.locationLists.map((array, index) =>
                                                    <option value={array._id}>{array.locationName}</option>
                                                )}

                                            </Form.Control>
                                        </Form.Group>
                                        <Form.Group>
                                        <Form.Label>Duration</Form.Label>
                                        <Form.Control type="number" placeholder="Duration"
                                            name='Duration'
                                            onChange={this.getInputTextValue}
                                            value={this.state.Duration}
                                        />
                                    </Form.Group>
                                                    <Form.Group >
                                                        <Form.Label>Appointment Notes</Form.Label>
                                                        <Form.Control as="textarea" rows="3"
                                                            name='appointmentNotes'
                                                            onChange={this.getInputTextValue}
                                                            value={this.state.appointmentNotes}
                                                        />
                                                    </Form.Group>
                                                  
                                        </Form>                      
                             
                                  </Modal.Body>
                                  <Modal.Footer>
                                    
                                    <Button variant="primary" onClick={this.updateBreakPeriod}>
                                      Update Break Point
                                    </Button>
                                    <Button variant="danger" onClick={() => this.delBreakData(this.state.breakId)}  >
                                      Delete
                                    </Button>
                                    <Button variant="secondary" onClick={this.handleBreakClose}>
                                      Close
                                    </Button>
                                  </Modal.Footer>
                                </Modal>
                                </Row>

                            </Card.Body>
                        </Card>

                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default CalendarView;