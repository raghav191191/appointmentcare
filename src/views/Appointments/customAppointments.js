import React from 'react';
import {Row, Col, Card, Form, Button, Modal} from 'react-bootstrap';
import { MDBDataTable } from 'mdbreact';
import { Link } from 'react-router-dom'
import TimePicker from 'react-bootstrap-time-picker';
import DatePicker from 'react-datepicker'
import 'mdbreact/dist/css/mdb.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import "react-datepicker/dist/react-datepicker.css";
import Aux from "../../hoc/_Aux";
import AppointmentService from '../../services/appointmentService'
import LocationService from '../../services/locationService'
import ServicesServices from '../../services/serviceServices'
import CustomerService from '../../services/customerService'
import StaffService from '../../services/staffService'

const Api = new AppointmentService()
const Lo_Api = new LocationService()
const Se_Api = new ServicesServices()
const Cu_Api = new CustomerService()
const St_Api = new StaffService()

let RowArray = [];
class CustomAppointments extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            locationLists: [],
            serviceLists: [],
            customerLists: [],
            appointmentLists:[],
            staffLists:[],
            editModel: false,
            smsConfirmation: false,
            appoinmentCourse: false,
            startDate: new Date(),
            time: 0,
            appoinmentCourse:false
        }
        this.handleClose = this.handleClose.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
    }

    componentDidMount() {
        this.getLocationList();
        this.getServicesList();
        this.getCustomerList();
       
        this.getStaffList();
    }
    addDays=()=>{
      var date = new Date()
      date.setDate(date.getDate() + 1);
      return date;
    }

    getLocationList = () => {
        const ownerId = Lo_Api.getProfile().id;
        Lo_Api.getLocationData(ownerId)
            .then((result) => {
                this.setState({
                    locationLists: result.data
                })
            }).catch(err => {
                console.log('xxx', err);
            })
    }
    getLocationById = valueId => {
        if (valueId) {
          let nameData = ''
          for (let i = 0; i < this.state.locationLists.length; i++) {
            if (valueId == this.state.locationLists[i]._id) {
              nameData =
                nameData + ' ' + this.state.locationLists[i].locationName 
            }
          }
          return nameData
        }
      }
    getServicesList = () => {
        const ownerId = Se_Api.getProfile().id;
        Se_Api.getServicesData(ownerId)
            .then((result) => {

                this.setState({
                    serviceLists: result.data
                })

            }).catch(err => {
                console.log('xxx', err);
            })
    }
    getServiceById = valueId => {
        if (valueId) {
          let nameData = ''
          for (let i = 0; i < this.state.serviceLists.length; i++) {
            if (valueId == this.state.serviceLists[i]._id) {
              nameData =
                nameData + ' ' + this.state.serviceLists[i].serviceName 
            }
          }
          return nameData
        }
      }
    getCustomerList = () => {
        const ownerId = Cu_Api.getProfile().id;
        Cu_Api.getCustomerData(ownerId)
            .then((result) => {
                this.setState({
                    customerLists: result.data
                })

            }).catch(err => {
                console.log('xxx', err);
            })
    }
    getCustomerById = valueId => {
        if (valueId) {
          let nameData = ''
          for (let i = 0; i < this.state.customerLists.length; i++) {
            if (valueId == this.state.customerLists[i]._id) {
              nameData =
              this.state.customerLists[i].customerFirstName + ' ' + this.state.customerLists[i].custmerLastName 
            }
          }
          return nameData
        }
      }
    getStaffList = () => {
        const ownerId = Lo_Api.getProfile().id;
        St_Api.getStaffList(ownerId)
            .then((result) => {
                this.setState({
                    staffLists: result.data
                })
                this.getAppointmentList();
            }).catch(err => {
                console.log('xxx', err);
            })
    }
    getStaffById = valueId => {
        if (valueId) {
          let nameData = ''
          for (let i = 0; i < this.state.staffLists.length; i++) {
            if (valueId == this.state.staffLists[i]._id) {
              nameData =
              this.state.staffLists[i].firstName + ' ' + this.state.staffLists[i].lastName 
            }
          }
          return nameData
        }
      }
      getMondayOfCurrentWeek=(d)=>
{
    var day = d.getDay();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate() + (day == 0?-6:1)-day );
}
getSundayOfCurrentWeek=(d)=>
{
    var day = d.getDay();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate() + (day == 0?0:7)-day );
}

    getAppointmentList=()=>{
        const ownerId = Api.getProfile().id;
        Api.getAppointmentData(ownerId)
            .then((result) => {
                RowArray = [];
                for (let i = 0; i < result.data.length; i++) {
                  RowArray.push({
                    appointmentDate: result.data[i].appointmentDate,
                    appointmentTime: result.data[i].appointmentTime,
                    serviceProvider: this.getStaffById(result.data[i].serviceProvider) ,
                    location:this.getLocationById(result.data[i].location) ,
                    CustomerId: this.getCustomerById(result.data[i].CustomerId),
                    Service: this.getServiceById(result.data[i].Service),
                    action: [
                      <span
                        className='iconSet' id="editResource" onClick={() => this.editResource(result.data[i])}
        
                      >
                        <i className="feather icon-edit"></i>
        
                      </span>, <span className='iconSet' id="delResource" onClick={() => this.submitCorfirm(result.data[i]._id)} >
                        <i className="feather icon-trash-2"></i>
        
                      </span>]
                  });
               
                }
                RowArray = RowArray.reverse()
                this.setState({ RowArray: RowArray });
              
            }).catch(err => {
                console.log('xxx', err);
            })
    }

    submitCorfirm = (id) => {
        confirmAlert({
          title: 'Confirm to Delete Appointment',
          message: 'Are you sure to do this.',
          buttons: [
            {
              label: 'Yes',
              onClick: () => this.delAppointmentData(id)
            },
            {
              label: 'No',
            }
          ]
        });
      };
    
      delAppointmentData = (id) => {
        Api.delAppointment(id)
          .then((result) => {
            this.getAppointmentList();
          }).catch(err => {
            console.log('xxx', err);
          })
      }

      editResource = (dataVo) => {
          console.log('xx-xx-dataVo', dataVo);
          let datev1=new Date(dataVo.appointmentDate)
          console.log('xx-xx-dataVo datev1', datev1);
        this.setState({
          editModel: !this.state.editModel,
          editId: dataVo._id,
           startDate:datev1,
           time:dataVo.appointmentTime,
          serviceProvider:dataVo.serviceProvider,
          location:dataVo.location,
          Service:dataVo.Service,
          Duration:dataVo.Duration,
          appointmentNotes:dataVo.appointmentNotes,
          appoinmentCourse:dataVo.appoinmentCourse,
          appointmentNameCourse:dataVo.appointmentNameCourse,
          limitedParticipants:dataVo.limitedParticipants,
          CustomerId:dataVo.CustomerId,
          smsConfirmation:dataVo.smsConfirmation
         
        })
      }

      editAppointmentSubmitBtn = () => {
        const dataVo = {
          id: this.state.editId,
          ownerId: Api.getProfile().id,
          appointmentDate: this.formatDate(this.state.startDate),
          appointmentTime:this.state.time,
          serviceProvider:this.state.serviceProvider,
          location:this.state.location,
          Service:this.state.Service,
          Duration:this.state.Duration,
          appointmentNotes:this.state.appointmentNotes,
          appoinmentCourse:this.state.appoinmentCourse,
          appointmentNameCourse:this.state.appointmentNameCourse,
          limitedParticipants:this.state.limitedParticipants,
          CustomerId:this.state.CustomerId,
          smsConfirmation:this.state.smsConfirmation
    
        }
        console.log('xxxxxx-xx-x-xxx-x',dataVo);
        
    
        Api.updateAppointmentInfoVo(dataVo)
          .then(result => {
            if (result.success) {
              this.getAppointmentList();
              this.setState({
                editModel: !this.state.editModel,
              })
            }
          }).catch(err => {
            console.log('err', err)
          })
      }

      formatDate = date => {
        const eventDate = new Date(date)
        const monthIndex = ('0' + (eventDate.getMonth() + 1)).slice(-2)
        const day = ('0' + eventDate.getDate()).slice(-2)
        const year = eventDate.getFullYear()
        return year + '-' + monthIndex + '-' + day
      }

      handleClose = () => {
        this.setState({
          editModel: !this.state.editModel,
        })
      }
      handleChange = date => {
          console.log('xxx',date);
          
        this.setState({
            startDate: date
        });
    };
    convertSecondstoTime=(given_seconds)=> { 
      
      let timeString=''
     let dateObj = new Date(given_seconds * 1000); 
     let hours = dateObj.getUTCHours(); 
     let minutes = dateObj.getUTCMinutes(); 
      let seconds = dateObj.getSeconds(); 

     return timeString = hours.toString().padStart(2, '0') 
          + ':' + minutes.toString().padStart(2, '0') 
         ; 

     
  } 

    handleTimeChange(time) {
      let AssignFrom=this.convertSecondstoTime(time)
           this.setState({ time:AssignFrom });
      }
        checkCustomerCheckBox = () => {
            this.setState({
                appoinmentCourse: !this.state.appoinmentCourse
            })
        }
        checkBoxChange=()=>{
            this.setState({
                smsConfirmation: !this.state.smsConfirmation
            })
        }
        getInputTextValue = event => {
            const target = event.target
            const value = target.value
            const name = target.name
            this.setState({
                [name]: value,
                isinvalid: '',
            })
        }

    render() {

        const data = {
            columns: [
              {
                label: 'appointment Date',
                field: 'appointmentDate',
                sort: 'asc',
                width: 150
              },
              {
                label: 'appointment Time',
                field: 'appointmentTime',
                sort: 'asc',
                width: 150
              },
              {
                label: 'service Provider',
                field: 'serviceProvider',
                sort: 'asc',
                width: 150
              },
            
              {
                label: 'Location',
                field: 'location',
                sort: 'asc',
                width: 150
              },
              
              {
                label: 'Customer',
                field: 'CustomerId',
                sort: 'asc',
                width: 150
              },
              {
                label: 'Service',
                field: 'Service',
                sort: 'asc',
                width: 150
              },
              {
                label: 'Action',
                field: 'action',
                sort: 'asc',
                width: 150
              },
            ],
            rows: RowArray
          }
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">All Appointments</Card.Title>
                            </Card.Header>
                            <Card.Body>
                          
                                <div>
                                <MDBDataTable
                                striped
                                data={data}
                                hover
                              />

                              <Modal show={this.state.editModel} size="lg"
                              aria-labelledby="contained-modal-title-vcenter">
                              <Modal.Header >
                                <Modal.Title>Appointment update</Modal.Title>
                              </Modal.Header>
                              <Modal.Body>

                              <Form>
                              <Row>
                                  <Col md={6}>
                                      <Form>
                                          <Form.Group controlId="appointmentDate">
                                              <Form.Label>Appointment Date</Form.Label>
                                              <DatePicker className="form-control"
                                                  selected={this.state.startDate}
                                                  onChange={this.handleChange}
                                                  minDate={this.addDays()}
                                              />
                                          </Form.Group>

                                          <Form.Group controlId="appointmentTime">
                                              <Form.Label>Appointment Time</Form.Label>

                                              <TimePicker format={24} start="06:00" end="23:00" step={30} onChange={this.handleTimeChange} value={this.state.time} readOnly   />
                                          </Form.Group>

                                          <Form.Group controlId="serviceProvider">
                                              <Form.Label>Service Provider</Form.Label>
                                              <Form.Control as="select" name="serviceProvider" onChange={this.getInputTextValue} value={this.state.serviceProvider}>

                                                  <option value={'*'}>Select provider</option>
                                                  {this.state.staffLists.map((array, index) =>
                                                      <option value={array._id}>{array.email}({array.firstName})</option>
                                                  )}

                                              </Form.Control>

                                          </Form.Group>
                                          <Form.Group controlId="Location">
                                              <Form.Label>Location</Form.Label>
                                              <Form.Control as="select" name="location" onChange={this.getInputTextValue} value={this.state.location}>
                                              <option value={'*'}>Select Location</option>
                                                  {this.state.locationLists.map((array, index) =>
                                                      <option value={array._id}>{array.locationName}</option>
                                                  )}

                                              </Form.Control>
                                          </Form.Group>
                                          <Form.Group controlId="Service">
                                              <Form.Label>Service</Form.Label>
                                              <Form.Control as="select" name="Service" onChange={this.getInputTextValue} value={this.state.Service}>
                                              <option value={'*'}>Select Service</option>
                                                  {this.state.serviceLists.map((array, index) =>
                                                      <option value={array._id}>{array.serviceName}</option>
                                                  )}

                                              </Form.Control>
                                          </Form.Group>
                                          <Form.Group controlId="Duration">
                                              <Form.Label>Duration</Form.Label>
                                              <Form.Control type="number" placeholder="Duration"
                                                  name='Duration'
                                                  onChange={this.getInputTextValue}
                                                  value={this.state.Duration}
                                              />
                                          </Form.Group>

                                      </Form>
                                  </Col>
                                  <Col md={6}>
                                      <Form.Group controlId="appointmentNotes">
                                          <Form.Label>Appointment Notes</Form.Label>
                                          <Form.Control as="textarea" rows="3"
                                              name='appointmentNotes'
                                              onChange={this.getInputTextValue}
                                              value={this.state.appointmentNotes}
                                          />
                                      </Form.Group>
                                      <Col md={12}>
                                          <h6 className="mt-5">Customer</h6>
                                          <hr />
                                          <Form.Group>
                                              <Form.Check
                                                  custom
                                                  type="checkbox"
                                                  id="checkbox2"
                                                  name="appoinmentCourse"
                                                  label="Register this appointment as a course"
                                                  onChange={this.checkCustomerCheckBox}
                                                  checked={this.state.appoinmentCourse}
                                              />
                                          </Form.Group>
                                          {this.state.appoinmentCourse ? (
                                              <div>
                                                  <Form.Group controlId="Name">
                                                      <Form.Label>Name</Form.Label>
                                                      <Form.Control type="text" placeholder="appointment_name_course"
                                                          name='appointmentNameCourse'
                                                          onChange={this.getInputTextValue}
                                                          value={this.state.appointmentNameCourse}
                                                      />
                                                  </Form.Group>
                                                  <Form.Group controlId="Service">
                                                      <Form.Label>Limited participants</Form.Label>
                                                      <Form.Control type="number" placeholder="Limited participants"
                                                          name='limitedParticipants'
                                                          onChange={this.getInputTextValue}
                                                          value={this.state.limitedParticipants}
                                                      />
                                                  </Form.Group>
                                              </div>
                                          ) : (
                                                  <Form.Group controlId="formCustomer">
                                                      <Form.Label>Select Customer</Form.Label>
                                                      <Form.Control as="select" name="CustomerId" onChange={this.getInputTextValue} value={this.state.CustomerId}>
                                                      <option value={'*'}>Select Customer</option>
                                                          {this.state.customerLists.map((array, index) =>
                                                              <option value={array._id}>{array.customerFirstName + ' ' + array.custmerLastName}</option>
                                                          )}

                                                      </Form.Control>
                                                      <Form.Text className="text-muted">
                                                          Please search for a customer, or  <Link to={'/AddCustomer'}> click here to add.</Link>
                                                      </Form.Text>
                                                  </Form.Group>
                                              )}

                                      </Col>

                                      <Col md={12}>
                                          <h6 className="mt-5">SMS Confirmation Options</h6>
                                          <hr />
                                          <Form.Group>
                                              <Form.Check
                                                  custom
                                                  type="checkbox"
                                                  id="checkbox1"
                                                  name="smsConfirmation"
                                                  label="Send SMS confirmation to customer?"
                                                  onChange={this.checkBoxChange}
                                                  checked={this.state.smsConfirmation}
                                              />
                                          </Form.Group>
                                      </Col>
                                     
                                  </Col>
                              </Row>
                          </Form>
                                
          
          
          
                              </Modal.Body>
                              <Modal.Footer>
                                <Button variant="secondary" onClick={this.handleClose}>
                                  Close
                                        </Button>
                                <Button variant="primary" onClick={this.editAppointmentSubmitBtn}>
                                  Update Changes
                                        </Button>
                              </Modal.Footer>
                            </Modal>
          
                                </div>
                              
                            </Card.Body>
                        </Card>
                       
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default CustomAppointments;
