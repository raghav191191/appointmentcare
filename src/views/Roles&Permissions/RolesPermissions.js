import React from 'react';
import {Row, Col, Card, Form, Button, InputGroup, FormControl, DropdownButton, Dropdown, Table} from 'react-bootstrap';

import Aux from "../../hoc/_Aux";

class RolesPermissions extends React.Component {

    render() {

        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                           
                            <Card.Header>
                                <Card.Title as="h5">Hover Table</Card.Title>
                                <span className="d-block m-t-5">View all roles and permissions</span>
                            </Card.Header>
                            <Card.Body>
                                <Table responsive hover>
                                    <thead>
                                    <tr>
                                        <th>Permissions</th>
                                        <th>Admin</th>
                                        <th>Scheduler</th>
                                        <th>Service Provider</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Has own calendar</th>
                                        <td>No</td>
                                        <td>No</td>
                                        <td>No</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Accept Appointments with Customers</th>
                                        <td>No</td>
                                        <td>No</td>
                                        <td>No</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">View Calendars</th>
                                        <td>Yes on all service providers'</td>
                                        <td>Yes on managed service providers'</td>
                                        <td>Yes, only their own</td>
                                    </tr>
                                    </tbody>
                                </Table>
                            </Card.Body>
                        </Card>
                              
                          
                       
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default RolesPermissions;
