import React from 'react';
import {Row, Col, Card, Form, Button, Modal} from 'react-bootstrap';
import { MDBDataTable } from 'mdbreact';
import 'mdbreact/dist/css/mdb.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import { confirmAlert } from 'react-confirm-alert'; 
import 'react-confirm-alert/src/react-confirm-alert.css';
import Aux from "../../hoc/_Aux";
import CustomerService from '../../services/customerService'
const Api = new CustomerService()
let RowArray = [];
class CustomerListing extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            lists:[],
            editModel:false,
            editReceiveEmailN:false,
            editReceiveSmsN:false
        }
       
        this.handleClose = this.handleClose.bind(this);
    }
    componentDidMount() {
        
            this.getCustomerList();
       
    }
    getInputTextValue = event => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
            [name]: value,
            isinvalid: '',
        })
    }
    getCustomerList = () => {
        const ownerId = Api.getProfile().id;
        Api.getCustomerData(ownerId)
            .then((result) => {
                RowArray = [];
                for(let i=0; i<result.data.length; i++){
                      RowArray.push({
                        customerEmail:result.data[i].customerEmail, 
                        customerName:result.data[i].customerFirstName+' '+result.data[i].custmerLastName,
                        homePhone:result.data[i].homePhone,
                        notes:result.data[i].notes,
                       
                           action:[
                           <span
                           className='iconSet'  onClick={() => this.editCustomer(result.data[i])}
                           
                         >
                         <i className="feather icon-edit"></i>
                         
                         </span>, <span  className='iconSet' onClick={() => this.submitCorfirm(result.data[i]._id)} > 
                         <i className="feather icon-trash-2"></i> 
                         
                         </span>]
                      });
              }
              RowArray = RowArray.reverse()
              this.setState({ RowArray: RowArray });
                
            }).catch(err => {
                console.log('xxx', err);
            })
    }
    submitCorfirm = (id) => {
        confirmAlert({
          title: 'Confirm to Delete Customer',
          message: 'Are you sure to do this.',
          buttons: [
            {
              label: 'Yes',
             onClick: () => this.delCustomer(id)
            },
            {
              label: 'No',
            }
          ]
        });
      };

      editCustomer=(dataVo)=>{
          
        this.setState({
          editModel:!this.state.editModel,
          editId:dataVo._id,
          editCustomerEmail:dataVo.customerEmail,
          editCustomerFirstName:dataVo.customerFirstName,
          editCustmerLastName:dataVo.custmerLastName,
          editHomePhone:dataVo.homePhone,
          editMobileNumber:dataVo.mobileNumber,
          editNotes:dataVo.notes,
          editReceiveSmsN:dataVo.receiveSmsN,
          editReceiveEmailN:dataVo.receiveEmailN,
         
        })
      }

      delCustomer=(id)=>{
        Api.delCustomerById(id)
        .then((result) => {
          this.getCustomerList(); 
       }).catch(err => {
           console.log('xxx', err);
       }) 
      }

      handleClose=()=>{
        this.setState({
            editModel:!this.state.editModel,
          })
      }

      checkBoxChange1 = () => {

        this.setState({
            editReceiveSmsN: !this.state.editReceiveSmsN
        })
      }
      checkBoxChange2 = () => {

        this.setState({
            editReceiveEmailN: !this.state.editReceiveEmailN
        })
      }

      editCustomerSubmit = () =>{
        const dataVo={
          id:this.state.editId,
          ownerId: Api.getProfile().id,
          customerEmail:this.state.editCustomerEmail,
          customerFirstName:this.state.editCustomerFirstName,
          custmerLastName:this.state.editCustmerLastName,

          homePhone:this.state.editHomePhone,
          mobileNumber:this.state.editMobileNumber,
          notes:this.state.editNotes,

          receiveSmsN:this.state.editReceiveSmsN,
          receiveEmailN:this.state.editReceiveEmailN,
         
         
        }
        
        Api.updateCustomerInfo(dataVo)
        .then(result=>{
       if(result.success){
         this.getCustomerList();
         this.setState({
          editModel:!this.state.editModel,
         })
       }
        }).catch(err=>{
          console.log('err',err)
        })
      }
                


    render() {

        const data = {
            columns: [
                {
                    label: 'Customer Email',
                    field: 'customerEmail',
                    sort: 'asc',
                    width: 150
                },
                  {
                    label: 'customer Name',
                    field: 'customerName',
                    sort: 'asc',
                    width: 150
                  },
                  {
                    label: 'homePhone',
                    field: 'homePhone',
                    sort: 'asc',
                    width: 150
                  },
                  {
                    label: 'Notes',
                    field: 'notes',
                    sort: 'asc',
                    width: 150
                  },
                 
                  {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 150
                  },
            ],
            rows: RowArray
        }

        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title as="h5">Customer Listing</Card.Title>
                            </Card.Header>
                            <Card.Body>
                          
                            <div>
                            <MDBDataTable
                                striped
                                data={data}
                                hover
                            />
                            <Modal show={this.state.editModel}>
                            <Modal.Header >
                              <Modal.Title>Customer update</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                            <Form>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Customer Email</Form.Label>
                                <Form.Control type="email" placeholder="Enter email"
                                    name='editCustomerEmail' value={this.state.editCustomerEmail}
                                    onChange={this.getInputTextValue} readOnly
                                />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group controlId="formBasicName">
                                <Form.Label>Customer Firstname</Form.Label>
                                <Form.Control type="text" placeholder="Firstname"
                                    name='editCustomerFirstName' value={this.state.editCustomerFirstName}
                                    onChange={this.getInputTextValue}
                                />
                            </Form.Group>

                            <Form.Group controlId="formBasicPhone">
                                <Form.Label>Customer Lastname</Form.Label>
                                <Form.Control type="text" placeholder="Lastname"
                                    name='editCustmerLastName' value={this.state.editCustmerLastName}
                                    onChange={this.getInputTextValue}
                                />
                            </Form.Group>
                            <Form.Group controlId="formBasicStreet1">
                                <Form.Label>Home Phone</Form.Label>
                                <Form.Control type="text" placeholder="phone Number"
                                     name='editHomePhone' value={this.state.editHomePhone}
                                    onChange={this.getInputTextValue}
                                />
                            </Form.Group>
                            <Form.Group controlId="formBasicStreet2">
                                <Form.Label>Mobile Number</Form.Label>
                                <Form.Control type="text" placeholder="Mobile number"
                                    name='editMobileNumber' value={this.state.editMobileNumber}
                                    onChange={this.getInputTextValue}
                                />
                            </Form.Group>
                            <Form.Group controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Notes</Form.Label>
                            <Form.Control as="textarea" rows="3"
                                name='editNotes' value={this.state.editNotes}
                                onChange={this.getInputTextValue}
                            />
                        </Form.Group>

                       
                            <h6 className="mt-5">Notification settings</h6>
                            <hr />
                            <Form.Group>
                                <Form.Check
                                    custom
                                    type="checkbox"
                                    id="checkbox1"
                                    name="editReceiveSmsN"
                                    label="Receive SMS notifications on appointment changes "
                                    onChange={this.checkBoxChange1}
                                    checked={this.state.editReceiveSmsN}
                                />
                            </Form.Group>


                            <Form.Group>
                                <Form.Check
                                    custom
                                    type="checkbox"
                                    id="checkbox2"
                                    name="editReceiveEmailN"
                                    label="Receive email notifications on appointment changes"
                                    onChange={this.checkBoxChange2}
                                    checked={this.state.editReceiveEmailN}
                                />
                            </Form.Group>

                        </Form>
                            
                                                          
                       
                            </Modal.Body>
                            <Modal.Footer>
                              <Button variant="secondary" onClick={this.handleClose}>
                                Close
                              </Button>
                              <Button variant="primary" onClick={this.editCustomerSubmit}>
                                Update Changes
                              </Button>
                            </Modal.Footer>
                          </Modal>
                            </div>
                              
                            </Card.Body>
                        </Card>
                       
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default CustomerListing;
