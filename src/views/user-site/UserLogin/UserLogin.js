import React from 'react';
import {NavLink} from 'react-router-dom';
import {Row, Col, Card, Form, Button, InputGroup,Alert, FormControl, DropdownButton, Dropdown} from 'react-bootstrap';

import './../../../assets/scss/style.scss';
import Aux from "../../../hoc/_Aux";
import Breadcrumb from "../../../App/layout/AdminLayout/Breadcrumb";
import AuthService from '../../../services/businessSiteUser/authService'
import { localDataSet } from '../../../config/localDataSet';


const AuthServiceApi = new AuthService()


class UserLogin extends React.Component {  

    constructor(props){
        super(props)
       
    }


    componentDidMount(){
      
    }
    
  getInputTextValue = event => {
    const target = event.target
    const value = target.value
    const name = target.name
    this.setState({
      [name]: value,
      isinvalid: ''
    })
  }

  loginMe=()=>{
    const userInfoVo = {
        siteOwnerId:this.props.userId,
        email: this.state.email,
        password: this.state.password
      }
  
      AuthServiceApi.loginInfo(userInfoVo)
        .then(result => {

            if (result.success) {
                this.setState({
                  actionMessage: result.message,
                  actionStatus: 'success',
                  alertBox: true,
                })
                
                setTimeout(() => {
                  this.setState({
                    alertBox: false
                  })
                }, 2000)
                if(result.token){
                    localDataSet.setLocal('token',result.token);
                    this.props.history.push('/business/dashboard') 
                }   
              } else {
                  
                this.setState({
                  actionMessage: result.message,
                  actionStatus: 'warning',
                  alertBox: true,
                })
              }
            
          
        })
        .catch(err => {
          console.log('err', err)
        })
}
    render () {
        return(
            <Aux>
                <Breadcrumb/>
                <div className="auth-wrapper">
                    <div className="auth-content">
                        <div className="auth-bg">
                            <span className="r"/>
                            <span className="r s"/>
                            <span className="r s"/>
                            <span className="r"/>
                        </div>
                        <div className="card">
                            <div className="card-body text-center">
                                <div className="mb-4">
                                    <i className="feather icon-unlock auth-icon"/>
                                </div>
                                <h3 className="mb-4">Login</h3>
        
                                <div className="input-group mb-3">
                                    <input type="email" className="form-control" placeholder="Email" name="email" onChange={this.getInputTextValue}/>
                                </div>
                                <div className="input-group mb-4">
                                    <input type="password" className="form-control" placeholder="password" name="password" onChange={this.getInputTextValue}/>
                                </div>
                                <div className="form-group text-left">
                                    <div className="checkbox checkbox-fill d-inline">
                                        <input type="checkbox" name="checkbox-fill-1" id="checkbox-fill-a1" />
                                            <label htmlFor="checkbox-fill-a1" className="cr"> Save credentials</label>
                                    </div>
                                </div>
                                <button className="btn btn-primary shadow-2 mb-4" onClick={this.loginMe}>Login</button>
                                <p className="mb-2 text-muted">Forgot password? <NavLink to="/auth/reset-password-1">Reset</NavLink></p>
                                <p className="mb-0 text-muted">Don’t have an account? <NavLink to={"/business/"+ this.props.directoryName +"/user/register"}>Register</NavLink></p>
                            </div>
                        </div>
                    </div>
                </div>
            </Aux>
        );
    }
}

export default UserLogin;