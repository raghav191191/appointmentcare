export default {
    items: [
        {
            id: 'navigation',
            title: 'Navigation',
            type: 'group',
            icon: 'icon-navigation',
            children: [
                {
                    id: 'dashboard',
                    title: 'Dashboard',
                    type: 'item',
                    url: '/business/dashboard',
                    icon: 'feather icon-home',
                },
                {
                    id: 'Calendar',
                    title: 'Calendar',
                    type: 'item',
                    url: '/business/dashboard',
                    icon: 'feather icon-calendar',
                },

                
            ]
        },
        
    ]
}