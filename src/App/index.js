import React, { Component, Suspense } from 'react';
import { Switch, Route,HashRouter} from 'react-router-dom';
import Loadable from 'react-loadable';

import '../../node_modules/font-awesome/scss/font-awesome.scss';

import Loader from './layout/Loader'
import Aux from "../hoc/_Aux";
import ScrollToTop from './layout/ScrollToTop';
import routes from "../route";

import SiteService from '../services/siteService';

const SiteServiceApi = new SiteService()

const AdminLayout = Loadable({
    loader: () => import('./layout/AdminLayout'),
    loading: Loader
});

const UserLayout = Loadable({
    loader: () => import('./layout/UserLayout'),
    loading: Loader
})
const Business = React.lazy(() => import('../Demo/Authentication/SignUp/SignUp1'));
const Signin1 = React.lazy(() => import('../Demo/Authentication/SignIn/SignIn1'));
const AppointmentSiteUi = React.lazy(() => import('../views/user-site/AppointmentSiteUi/AppointmentSiteUi'));
const UserLogin = React.lazy(() => import('../views/user-site/UserLogin/UserLogin'));
const UserRegister = React.lazy(() => import('../views/user-site/UserRegister/UserRegister'));

class App extends Component {

    constructor(props) {
        super(props)
        this.state = {
            directoryList: []
        }
    }

    componentDidMount() {
        SiteServiceApi.getDirectoryList()
            .then(result => {

                this.setState({
                    directoryList: result.data
                })

            })
    }
    render() {
        console.log('directoryList',this.state.directoryList)
        const menu = routes.map((route, index) => {
            return (route.component) ? (
                <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    render={props => (
                        <route.component {...props} />
                    )} />
            ) : (null);
        });

        if (!(this.state.directoryList.length == 0)) {
            console.log('fulll', this.state.directoryList)
        }

        return (

            <HashRouter>

                {!(this.state.directoryList.length == 0) ? <ScrollToTop>
                    <Suspense fallback={<Loader />}>
                        <Switch>
                            {/* {menu} */}
                            {this.state.directoryList.map(value => (
                                <Route path={'/' + value.directoryName}
                                    render={props => (
                                        <AppointmentSiteUi {...props} userId={value._id} />
                                    )} />
                            ))}

                            {this.state.directoryList.map(value => (
                                 <Route path={'/business/' + value.directoryName + '/user/login'}
                                    render={props => (
                                        <UserLogin {...props} userId={value._id} directoryName={value.directoryName} />
                                    )} />
                            ))}
                            {this.state.directoryList.map(value => (
                                <Route path={'/business/' + value.directoryName + '/user/register'}
                                render={props => (
                                    <UserRegister {...props} userId={value._id} directoryName={value.directoryName} />
                                )} />
                            ))}
                            
                            <Route path="/auth/business" component={Business} />
                            <Route path="/auth/signin-1" component={Signin1} />
                            <Route path="/business" component={UserLayout} />
                            <Route path="/" component={AdminLayout} />
                            
                        </Switch>
                    </Suspense>
                </ScrollToTop> : ''}

            </HashRouter>
        );
    }
}

export default App;
